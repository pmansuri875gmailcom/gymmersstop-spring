package com.gymmersstop.common.service;

import java.util.List;

import com.gymmersstop.common.dto.RoleDTO;
import com.gymmersstop.exception.DuplicateRecordException;

public interface RoleServiceInterface {
	/**
	 * Adds a Role.
	 * 
	 * param dto
	 * return
	 * throws DuplicateRecordException
	 */
	public long add(RoleDTO dto) throws DuplicateRecordException;

	/**
	 * Updates a Role.
	 * 
	 * param dto
	 * throws DuplicateRecordException
	 */
	public void update(RoleDTO dto) throws DuplicateRecordException;

	/**
	 * Deletes a Role
	 * 
	 * param id
	 */
	public void delete(long id);

	/**
	 * Finds a Role by name.
	 * 
	 * param roleName
	 * return
	 */
	public RoleDTO findByName(String roleName);

	/**
	 * Finds a Role by ID
	 * 
	 * param id
	 * return
	 */
	public RoleDTO findById(long id);

	/**
	 * Searches Roles with pagination.
	 * 
	 * param dto
	 * param pageNo
	 * param pageSize
	 * return
	 */
	public List<RoleDTO> search(RoleDTO dto, int pageNo, int pageSize);

	/**
	 * Searches Roles
	 * 
	 * param dto
	 * return
	 */
	public List<RoleDTO> search(RoleDTO dto);
}

