package com.gymmersstop.common.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.common.dao.RoleDAOInterface;
import com.gymmersstop.common.dao.UserDAOInterface;
import com.gymmersstop.common.dto.UserDTO;

@Service
public class UserServiceSpringImplementation implements UserServiceInterface {

	@Autowired
	private UserDAOInterface dao;

	private static Logger log = Logger.getLogger(UserServiceSpringImplementation.class);

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public long add(UserDTO dto) {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(UserDTO dto) {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserDTO findByLogin(String login) {
		return dao.findByLogin(login);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserDTO findByPk(long pk) {
		return dao.findByPk(pk);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public long update(UserDTO dto) {
		return dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List search(UserDTO dto) {
		return dao.search(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List search(UserDTO dto, int pageNo, int pageSize) {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List list() {
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List gymmerList() {
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List list(int pageNo, int pageSize) {
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserDTO authenticate(String login, String password) {
		return dao.authenticate(login, password);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List getRoles(UserDTO dto) {
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public boolean changePassword(long id, String oldPassword, String newPassword) {
		// TODO Auto-generated method stub
		return dao.changePassword(id, oldPassword, newPassword);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserDTO updateAccess(UserDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public long registerAdmin(UserDTO dto) {
		// TODO Auto-generated method stub
		return dao.registerAdmin(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public boolean resetPassword(UserDTO dto) {
		// TODO Auto-generated method stub
		return dao.resetPassword(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public boolean forgetPassword(String login) {
		// TODO Auto-generated method stub
		return dao.forgetPassword(login);
	}

}
