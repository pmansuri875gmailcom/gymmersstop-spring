package com.gymmersstop.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gymmersstop.common.dto.UserDTO;


public interface UserDAOInterface {

	public long add(UserDTO dto);
	public void delete(UserDTO dto);
	public UserDTO findByLogin(String login);
	public UserDTO findByPk(long pk);
	public long update(UserDTO dto);
	public List search(UserDTO dto);
	public List search(UserDTO dto, int pageNo , int pageSize);
	public List list();
	public List gymmerList();
	public List list(int pageNo , int pageSize);
	public UserDTO authenticate(String login , String password);
	public List getRoles(UserDTO dto);
	public boolean changePassword(long id , String oldPassword , String newPassword);
	public UserDTO updateAccess(UserDTO dto);
	public long registerAdmin(UserDTO dto);
	public boolean resetPassword(UserDTO dto);
	public boolean forgetPassword(String login);
	
}
