package com.gymmersstop.common.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.naming.NamingException;
import javax.naming.Reference;
import org.apache.log4j.Logger;
import org.hibernate.Cache;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Interceptor;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.TypeHelper;
import org.hibernate.classic.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.FilterDefinition;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.stat.Statistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.common.dto.RoleDTO;

@Repository(value = "roleDao")
public class RoleDAOImplementation implements RoleDAOInterface {

	/** The sessionfactory. */
	@Autowired
	private HibernateTemplate ht;

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}

	/** The log. */
	private static Logger log = Logger.getLogger(RoleDAOImplementation.class);

	@Override
	public long add(RoleDTO dto) {
		log.debug("RoleDAOHibImpl Add Started");
		long pk = 0;
		try {
			pk = (Long) ht.save(dto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("RoleDAOHibImpl Add End");
		return pk;
	}

	public void update(RoleDTO dto) {
		log.debug("RoleDAOHibImpl Update Started");
		ht.update(dto);
		log.debug("RoleDAOHibImpl Update End");

	}

	public void delete(long id) {
		log.debug("RoleDAOHibImpl Delete Started");
		RoleDTO dto = findByPK(id);
		ht.delete(dto);
		log.debug("RoleDAOHibImpl Delete End");

	}

	public RoleDTO findByName(String roleName) {
		log.debug("RoleDAOHibImpl Find by Name Started");
		RoleDTO dto = null;
		DetachedCriteria dc = DetachedCriteria.forClass(RoleDAOImplementation.class);
		dc.add(Restrictions.eq("name", roleName));
		List list = ht.findByCriteria(dc);
		dto = (RoleDTO) list.get(0);
		return dto;
	}

	public RoleDTO findByPK(long pk) {
		log.debug("RoleDAOHibImpl Find by PK Started");
		RoleDTO dto = null;
		dto = (RoleDTO) ht.get(RoleDTO.class, pk);
		log.debug("RoleDAOHibImpl Find by PK Ended");
		return dto;
	}

	public List<RoleDTO> search(RoleDTO dto, int pageNo, int pageSize) {
		log.debug("RoleDAOHibImpl search Started");
		List list = null;

		DetachedCriteria ds = DetachedCriteria.forClass(RoleDTO.class);

		if (dto != null) {

			if (dto.getId() > 0) {
				ds.add(Restrictions.eq("id", dto.getId()));
			}
			if (dto.getName() != null && dto.getName().length() > 0) {
				ds.add(Restrictions.like("name", dto.getName() + "%"));
			}
			if (dto.getDescription() != null && dto.getDescription().length() > 0) {
				ds.add(Restrictions.like("description", dto.getDescription() + "%"));
			}
		}
		// if page size is greater than zero the apply pagination

		if (pageSize > 0) {
			pageNo = (pageNo - 1) * pageSize;

		}
		list = ht.findByCriteria(ds, pageNo, pageSize);
		log.debug("RoleDAOHibImpl search End");
		return list;
	}

	public List<RoleDTO> search(RoleDTO dto) {
		return search(dto, 0, 0);
	}

}
