package com.gymmersstop.common.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.omg.PortableInterceptor.USER_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.common.dto.UserDTO;
import com.gymmersstop.exception.DuplicateRecordException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.util.DataValidator;
import com.gymmersstop.util.EmailBuilder;
import com.gymmersstop.util.EmailMessage;
import com.gymmersstop.util.EmailUtility;

@Repository("userDao")
public class UserDAOImplementation implements UserDAOInterface {
	/** The sessionfactory. */
	@Autowired
	private HibernateTemplate ht = null;

	/** The log. */
	private static Logger log = Logger.getLogger(UserDAOImplementation.class);

	@Override
	public long add(UserDTO dto) {
		long pk = (long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(UserDTO dto) {
		ht.delete(dto);
	}

	@Override
	public UserDTO findByLogin(String login) {
		UserDTO dto = null;
		try {
			DetachedCriteria dc = DetachedCriteria.forClass(UserDTO.class);
			dc.add(Restrictions.eq("login", login));
			List list = ht.findByCriteria(dc);
			dto = (UserDTO) list.get(0);
		} catch (NoRecordFoundException e) {
			e.printStackTrace();

		}
		return dto;
	}

	@Override
	public UserDTO findByPk(long pk) {
		UserDTO dto = (UserDTO) ht.get(UserDTO.class, pk);
		return dto;
	}

	@Override
	public long update(UserDTO dto) {
		ht.update(dto);
		return dto.getId();
	}

	@Override
	public List search(UserDTO dto) {

		return search(dto, 0, 0);
	}

	@Override
	public List search(UserDTO dto, int pageNo, int pageSize) {
		DetachedCriteria dc = DetachedCriteria.forClass(UserDTO.class);
		if (dto != null) {
			if (dto.getId() > 0) {
				dc.add(Restrictions.eq("id", dto.getId()));
			}
			if (dto.getName() != null) {
				dc.add(Restrictions.like("name", dto.getName() + "%"));
			}
			if (dto.getRoleId() == AppRole.GYMMEMBER || dto.getRoleId() == AppRole.STAFF) {
				dc.add(Restrictions.like("name", dto.getRoleId() + "%"));
			}
			if (DataValidator.isNotNull(dto.getEmail())) {
				dc.add(Restrictions.like("name", dto.getName() + "%"));
			}
			if (DataValidator.isNotNull(dto.getMobileNo())) {
				dc.add(Restrictions.like("mobileNo", dto.getMobileNo() + "%"));
			}
			if (DataValidator.isNotNull(dto.getGender())) {
				dc.add(Restrictions.like("gender", dto.getGender() + "%"));
			}
		}
		if (pageSize > 0) {
			pageNo = (pageNo - 1) * pageSize;
		}
		List list = ht.findByCriteria(dc, pageNo, pageSize);
		log.debug("UserDaoImplementation search End");
		return list;

	}

	@Override
	public List gymmerList() {

		return null;
	}

	/*
	 * @Override public List list(int pageNo, int pageSize) { Criteria c =
	 * sessionfactory.getCurrentSession().createCriteria(UserDAOImplementation.
	 * class); if (pageSize > 0) { c.setFirstResult(((pageNo - 1) * pageSize));
	 * c.setMaxResults(pageSize); } List list = c.list(); return list; }
	 */
	@Override
	public UserDTO authenticate(String login, String password) {
		UserDTO dto = null;
		try {
			DetachedCriteria dc = DetachedCriteria.forClass(UserDTO.class);
			dc.add(Restrictions.eq("login", login));
			dc.add(Restrictions.eq("password", password));
			List list = ht.findByCriteria(dc);
			dto = (UserDTO) list.get(0);
		} catch (NoRecordFoundException e) {
			e.printStackTrace();
		}

		return dto;
	}

	@Override
	public List getRoles(UserDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean changePassword(long id, String oldPassword, String newPassword) {
		UserDTO dtoExist = findByPk(id);
		boolean flag = false;
		try {
			if (dtoExist != null && dtoExist.getPassword().equals(oldPassword)) {
				dtoExist.setPassword(newPassword);
				ht.update(dtoExist);
				flag = true;

				HashMap<String, String> map = new HashMap<String, String>();
				map.put("login", dtoExist.getEmail());
				map.put("password", dtoExist.getPassword());
				map.put("firstName", dtoExist.getName());

				String message = EmailBuilder.getChangePasswordMessage(map);
				EmailMessage msg = new EmailMessage();

				msg.setTo(dtoExist.getEmail());
				msg.setSubject("GYMMERSSTOP Password has been changed Successfully");
				msg.setMessage(message);
				msg.setMessageType(EmailMessage.HTML_MSG);

				EmailUtility.sendMail(msg);

			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return flag;
	}

	@Override
	public UserDTO updateAccess(UserDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long registerAdmin(UserDTO dto) {
		long pk = add(dto);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("login", dto.getEmail());
		map.put("password", dto.getPassword());

		String message = EmailBuilder.getAdminRegistrationMessage(map);
		EmailMessage msg = new EmailMessage();

		msg.setTo(dto.getEmail());
		msg.setSubject("Registration is successful for Gymmers Stop");
		msg.setMessage(message);
		msg.setMessageType(EmailMessage.HTML_MSG);

		EmailUtility.sendMail(msg);
		log.debug("UserModel registerUser End");
		return pk;

	}

	@Override
	public boolean resetPassword(UserDTO dto) {
		String newPassword = String.valueOf(new java.util.Date().getTime()).substring(0, 4);
		UserDTO userdto = findByPk(dto.getId());
		dto.setPassword(newPassword);
		try {
			update(userdto);
		} catch (DuplicateRecordException e) {
			return false;
		}

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("login", userdto.getEmail());
		map.put("password", userdto.getPassword());
		map.put("firstName", userdto.getName());

		String message = EmailBuilder.getForgetPasswordMessage(map);

		EmailMessage msg = new EmailMessage();

		msg.setTo(userdto.getEmail());
		msg.setSubject("Password has been reset");
		msg.setMessage(message);
		msg.setMessageType(EmailMessage.HTML_MSG);

		EmailUtility.sendMail(msg);
		log.debug("UserModel resetPassword End");
		return true;
	}

	@Override
	public boolean forgetPassword(String login) {
		UserDTO dto = findByLogin(login);
		boolean flag = false;
		if (dto == null) {
			throw new NoRecordFoundException("Email ID does not exists !");
		}
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("login", dto.getEmail());
		map.put("password", dto.getPassword());
		map.put("firstName", dto.getName());

		String message = EmailBuilder.getForgetPasswordMessage(map);

		EmailMessage msg = new EmailMessage();

		msg.setTo(login);
		msg.setSubject("GYMMERSSTOP Password  reset");
		msg.setMessage(message);
		msg.setMessageType(EmailMessage.HTML_MSG);
		EmailUtility.sendMail(msg);
		flag = true;
		log.debug("UserModel forgetPassword end");
		return flag;
	}

	@Override
	public List list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List list(int pageNo, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

}
