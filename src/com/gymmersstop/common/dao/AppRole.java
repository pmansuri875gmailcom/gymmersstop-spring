package com.gymmersstop.common.dao;

public interface AppRole {

	public int GUEST = 0;
	public int GYMMEMBER = 1;
	public int STAFF = 2;
	public int TRAINER = 3;
	public int GDVISITOR = 4;
	public int GDGYMADMIN = 5;
	public int ADMIN = 99;
}
