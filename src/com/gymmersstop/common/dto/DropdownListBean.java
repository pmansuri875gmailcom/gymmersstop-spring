package com.gymmersstop.common.dto;

/**
 * DropdownListBean interface is implemented by Beans those are used to
 * create Drop down list on html page
 * @author Tofique Ahmed Khan
 * @version 1.0
 */
public interface DropdownListBean {

	/**
	 * return key of list element
	 * @return
	 */
	public String getKey();
	
	/**
	 * return display text of list element
	 * @return
	 */
	public String getValue();
	
}
