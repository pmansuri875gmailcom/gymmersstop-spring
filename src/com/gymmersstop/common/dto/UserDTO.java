package com.gymmersstop.common.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name = "gms_user" )
public class UserDTO extends BaseDTO {

	/**
	 * Lock Active constant for User
	 */
	public static final String ACTIVE = "Active";

	/**
	 * Lock InActive constant for User
	 */
	public static final String INACTIVE = "Inactive";

	private static final long serialVersionUID = 1L;
	@Column(length = 255)
	private String name = null;
	@Column(length = 255)
	private String address = null;
	@Column(length = 255)
	private String mobileNo = null;
	@Column(length = 255)
	private String password = null;
	@Column(length = 255)
	private String confirmPassword = null;
	@Column(length = 255)
	private Date dateOfBirth = null;
	@Column(length = 255)
	private String gender = null;
	@Column(length = 255)
	private Timestamp lastLogin = null;
	@Column(length = 255)
	private String email = null;
	@Column(length = 255)
	private String userLock = INACTIVE;
	@Column 
	private long gymId;
	public String getUserLock() {
		return userLock;
	}

	public void setUserLock(String userLock) {
		this.userLock = userLock;
	}

	/**
	 * IP Address of User from where User was registered.
	 */
	private String registeredIp = null;
	/**
	 * IP Address of User of his last login
	 */
	private String lastLoginIp = null;
	/**
	 * Role of User
	 */
	private long roleId = 0;
	/**
	 * Number of unsuccessful login attempt
	 */
	private int unsuccessfulLogin = 0;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}


	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Timestamp getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getRegisteredIp() {
		return registeredIp;
	}

	public void setRegisteredIp(String registeredIp) {
		this.registeredIp = registeredIp;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public int getUnsuccessfulLogin() {
		return unsuccessfulLogin;
	}

	public void setUnsuccessfulLogin(int unsuccessfulLogin) {
		this.unsuccessfulLogin = unsuccessfulLogin;
	}

	
	@Override
	public String getKey() {
		return id + "";
	}

	@Override
	public String getValue() {
		return name;
	}

	public void setGymId(long gymId) {
		this.gymId = gymId;
	}
	public long getGymId() {
		return gymId;
	}
		

}
