package com.gymmersstop.gms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gms.dao.GymMemeberDaoInterface;
import com.gymmersstop.gms.dto.GymMemberDTO;

public class GymMemberServiceImplementation implements GymMemberServiceInterface {

	@Autowired
	private GymMemeberDaoInterface dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public long add(GymMemberDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(GymMemberDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(GymMemberDTO dto) throws ApplicationException {
		dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public GymMemberDTO findByPk(long id) throws ApplicationException {
		return dao.findByPk(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public GymMemberDTO findByUserId(long id) throws ApplicationException {
		return dao.findByUserId(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List search(GymMemberDTO dto, int pageNo, int pageSize) throws ApplicationException {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List search(GymMemberDTO dto) throws ApplicationException {
		return dao.search(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public long registerGymMember(GymMemberDTO dto) throws ApplicationException {
		return dao.registerGymMember(dto);
	}

}
