package com.gymmersstop.gms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gms.dao.GymMarketingDaoInterface;
import com.gymmersstop.gms.dto.GymMarketingDTO;

public class GymMarketingServiceSpringImplementation implements GymMarketingServiceInterface {

	@Autowired
	private GymMarketingDaoInterface dao;

	@Override
	@Transactional(propagation =Propagation.REQUIRES_NEW , readOnly = false)
	public long add(GymMarketingDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED, readOnly = false)
	public void delete(GymMarketingDTO dto) throws ApplicationException {
dao.delete(dto);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED, readOnly = false)
	public void update(GymMarketingDTO dto) throws ApplicationException {
		dao.update(dto);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED, readOnly = false)
	public GymMarketingDTO findByPk(long id) throws ApplicationException {
		return dao.findByPk(id);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED, readOnly = false)
	public List search(GymMarketingDTO dto, int pageNo, int pageSize) throws ApplicationException {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	public List search(GymMarketingDTO dto) throws ApplicationException {
		return dao.search(dto);
	}

}
