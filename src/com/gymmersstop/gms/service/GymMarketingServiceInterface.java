package com.gymmersstop.gms.service;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gms.dto.GymMarketingDTO;

public interface GymMarketingServiceInterface {
	public long add(GymMarketingDTO dto) throws ApplicationException;

	public void delete(GymMarketingDTO dto) throws ApplicationException;

	public void update(GymMarketingDTO dto) throws ApplicationException;

	public GymMarketingDTO findByPk(long id) throws ApplicationException;

	public List search(GymMarketingDTO dto, int pageNo, int pageSize) throws ApplicationException;

	public List search(GymMarketingDTO dto) throws ApplicationException;
}
