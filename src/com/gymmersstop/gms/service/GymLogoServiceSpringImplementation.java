package com.gymmersstop.gms.service;

import org.apache.commons.io.filefilter.FalseFileFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dao.GymLogoDaoInterface;
import com.gymmersstop.gms.dto.GymLogoDTO;

public class GymLogoServiceSpringImplementation implements GymLogoServiceInterface {

	@Autowired
	private GymLogoDaoInterface dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public long add(GymLogoDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(GymLogoDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(GymLogoDTO dto) throws ApplicationException {
		dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public GymLogoDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		return dao.findByPk(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public GymLogoDTO findByGymId(long gymId) throws ApplicationException {
		return dao.findByGymId(gymId);
	}

}
