package com.gymmersstop.gms.service;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dto.GymDTO;
import com.gymmersstop.gms.dto.StaffDTO;

public interface StaffServiceInterface {
	public long add(StaffDTO dto)throws ApplicationException;
	public void delete(StaffDTO dto)throws ApplicationException;
	public void update(StaffDTO dto)throws ApplicationException;
	public StaffDTO findByPk(long pk)throws ApplicationException,NoRecordFoundException;
	public StaffDTO findByUserId(long userId)throws ApplicationException,NoRecordFoundException;
	public List search(StaffDTO dto,int pageNo,int pageSize) throws ApplicationException;
	public List search(StaffDTO dto)throws ApplicationException;
	public long registerStaff(StaffDTO dto)throws ApplicationException;
}
