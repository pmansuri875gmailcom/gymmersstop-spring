package com.gymmersstop.gms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dao.StaffDaoInterface;
import com.gymmersstop.gms.dto.GymDTO;
import com.gymmersstop.gms.dto.StaffDTO;

public class StaffServiceSpringImplementation implements StaffServiceInterface {

	@Autowired
	private StaffDaoInterface dao;

	@Override
	@Transactional(propagation= Propagation.REQUIRES_NEW , readOnly = false)
	public long add(StaffDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation= Propagation.REQUIRED , readOnly = false)
	public void delete(StaffDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation= Propagation.REQUIRED , readOnly = false)
	public void update(StaffDTO dto) throws ApplicationException {
		dao.update(dto);
	}

	@Override
	@Transactional(propagation= Propagation.REQUIRED , readOnly = false)
	public StaffDTO findByPk(long pk) throws ApplicationException, NoRecordFoundException {
		return dao.findByPk(pk);
	}

	@Override
	@Transactional(propagation= Propagation.REQUIRED , readOnly = false)
	public StaffDTO findByUserId(long userId) throws ApplicationException, NoRecordFoundException {
		return dao.findByUserId(userId);
	}

	@Override
	@Transactional(propagation= Propagation.REQUIRED , readOnly = false)
	public List search(StaffDTO dto, int pageNo, int pageSize) throws ApplicationException {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	@Transactional(propagation= Propagation.REQUIRED , readOnly = false)
	public List search(StaffDTO dto) throws ApplicationException {
		return dao.search(dto);
	}

	@Override
	@Transactional(propagation= Propagation.REQUIRES_NEW , readOnly = false)
	public long registerStaff(StaffDTO dto) throws ApplicationException {
		return dao.registerStaff(dto);
	}
	

}
