package com.gymmersstop.gms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymTimingDTO;
import com.gymmersstop.gms.dao.GymGalleryDaoInterface;
import com.gymmersstop.gms.dto.GymGalleryDTO;

public class GymGalleryServiceSpringImplementation implements GymGalleryServiceInterface {

	@Autowired
	private GymGalleryDaoInterface dao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW , readOnly = false)
	public long add(GymGalleryDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public void delete(GymGalleryDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public long update(GymGalleryDTO dto) throws ApplicationException {
		return dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public GymGalleryDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		return dao.findByPk(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public List findByGymId(long id) throws ApplicationException, NoRecordFoundException {
		return dao.findByGymId(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public List search(GymGalleryDTO dto, int pageNo, int pageSize) throws ApplicationException {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public List search(GymGalleryDTO dto) throws ApplicationException {
		
		return dao.search(dto);
	}

}
