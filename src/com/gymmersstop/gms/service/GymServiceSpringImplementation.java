package com.gymmersstop.gms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dao.GymDaoInterface;
import com.gymmersstop.gms.dto.GymDTO;

public class GymServiceSpringImplementation implements GymServiceInterface {

	@Autowired
	private GymDaoInterface dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public long add(GymDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(GymDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED ,  readOnly = false)
	public void update(GymDTO dto) throws ApplicationException {
dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED ,  readOnly = false)
	public GymDTO findByPk(long pk) throws ApplicationException, NoRecordFoundException {
		return dao.findByPk(pk);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED ,  readOnly = false)
	public GymDTO findByLogin(String email) throws ApplicationException, NoRecordFoundException {
		return dao.findByLogin(email);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED ,  readOnly = false)
	public List search(GymDTO dto, int pageNo, int pageSize) throws ApplicationException {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED ,  readOnly = false)
	public List search(GymDTO dto) throws ApplicationException {
		return dao.search(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED ,  readOnly = false)
	public GymDTO authenticate(String email, String password) throws ApplicationException {
		return dao.authenticate(email, password);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED ,  readOnly = false)
	public long registerGym(GymDTO dto) throws ApplicationException {
		return dao.registerGym(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW,  readOnly = false)
	public long registerGymListing(GymDTO dto) throws ApplicationException {
		return dao.registerGymListing(dto);
	}

}
