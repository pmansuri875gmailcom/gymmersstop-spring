package com.gymmersstop.gms.service;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dto.GymLogoDTO;

public interface GymLogoServiceInterface {
	public long add(GymLogoDTO dto) throws ApplicationException;

	public void delete(GymLogoDTO dto) throws ApplicationException;

	public void update(GymLogoDTO dto) throws ApplicationException;

	public GymLogoDTO findByPk(long id) throws ApplicationException, NoRecordFoundException;
	
	public GymLogoDTO findByGymId(long gymId)throws ApplicationException;
}
