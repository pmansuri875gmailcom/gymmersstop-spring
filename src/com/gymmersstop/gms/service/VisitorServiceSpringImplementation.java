package com.gymmersstop.gms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gms.dao.VisitorDaoInterface;
import com.gymmersstop.gms.dto.VisitorDTO;

public class VisitorServiceSpringImplementation implements VisitorServiceInterface {
	@Autowired
	private VisitorDaoInterface dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)

	public long add(VisitorDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(VisitorDTO dto) throws ApplicationException {
		dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(VisitorDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public VisitorDTO findByPk(long pk) throws ApplicationException {
		return dao.findByPk(pk);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List search(VisitorDTO dto, int pageNo, int pageSize) throws ApplicationException {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	public List search(VisitorDTO dto) throws ApplicationException {
		return dao.search(dto);
	}

}
