package com.gymmersstop.gms.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


import com.gymmersstop.common.dto.BaseDTO;
@Entity
@Table(name = "gms_gym_attendance")
public class AttendanceDTO extends BaseDTO {
	/**
	Attendance Constant.
	*/
	public static final String PRESENT = "Present";
	public static final String ABSENT = "Absent";
	
	@Column
	private long userId = 0;
	@Column(length = 255)
	private String month = null;
	@Column(length = 255)
	private String day = ABSENT;
	

	@Override
	public String getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

}
