package com.gymmersstop.gms.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.BaseDTO;
@Entity
@Table(name ="gms_visitor")
public class VisitorDTO extends BaseDTO {
	@Column
	private String name = null;
	@Column(length = 255)
	private String gender = null;
	@Column(length = 255)
	private String address = null;
	@Column(length = 255)
	private String mobileNo = null;
	@Column(length = 255)
	private String email = null;
	@Column(length = 255)
	private String goal = null;
	@Column
	private long gymId = 0;
	

	public String getName() {
		return name;
	}

	public long getGymId() {
		return gymId;
	}

	public void setGymId(long gymId) {
		this.gymId = gymId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGoal() {
		return goal;
	}

	public void setGoal(String goal) {
		this.goal = goal;
	}


	@Override
	public String getKey() {
		return id +"";
	}

	@Override
	public String getValue() {
		return name;
	}

}
