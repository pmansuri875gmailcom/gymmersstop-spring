package com.gymmersstop.gms.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.BaseDTO;

@Entity
@Table(name = "gms_gym_gymlogo")

public class GymLogoDTO extends BaseDTO{
	@Column
	private long gymId;
	@Column(length = 255)
	private String logoName;

	public long getGymId() {
		return gymId;
	}

	public void setGymId(long gymId) {
		this.gymId = gymId;
	}

	public String getLogoName() {
		return logoName;
	}

	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}


	@Override
	public String getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

}
