package com.gymmersstop.gms.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.UserDTO;
@Entity
@Table(name ="gms_staff")
public class StaffDTO extends UserDTO {
	@Column
	private long userId = 0;
	@Column(length = 255)
	private Date joiningDate = null;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long staffId) {
		this.userId = staffId;
	}
	
	public Date getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}
	@Override
	public String getKey() {
		return id+"";
	}
	@Override
	public String getValue() {
		// Curently Null but we have to think about it.
		return null;
	}
}
