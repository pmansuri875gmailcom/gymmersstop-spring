package com.gymmersstop.gms.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.BaseDTO;
@Entity
@Table(name ="gms_gym_membership")
public class GymMembershipDTO extends BaseDTO{
	
	@Column
	private long userId = 0;
	@Column(length = 255)
	private String session = null;
	@Column
	private int membershipDays = 0;
	@Column
	private long gymId = 0;
	

	public long getGymId() {
		return gymId;
	}

	public void setGymId(long gymId) {
		this.gymId = gymId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public int getMembershipDays() {
		return membershipDays;
	}

	public void setMembershipDays(int membershipDays) {
		this.membershipDays = membershipDays;
	}


	@Override
	public String getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

}
