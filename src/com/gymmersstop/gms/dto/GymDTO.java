package com.gymmersstop.gms.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.BaseDTO;
@Entity
@Table(name =  "gms_gym_registration")
public class GymDTO extends BaseDTO{
	
	
	public static final String AVAILABLE = "available";
	public static final String NOTAVAILABLE = "notavailable";
	
	public static final String GYM = "gym";
	
	public static final String YOGA = "yoga";
	
	public static final String ZUMBA = "zumba";
	
	public static final String CROSS_FIT = "crossfit";
	
	public static final String AEROBICS = "aerobics";
	

	/**
	 * Gym Registered constant for Gym
	 */
	public static final String REGISTERED = "Registered";

	/**
	 * Gym NotRegistered constant for Gym
	 */
	public static final String NOTREGISTERED = "Notregistered";
	@Column(length = 255)
	private String gymName = null;
	@Column(length = 255)
	private String address = null;
	@Column(length = 255)
	private String ownerName = null;
	@Column(length = 255)
	private String mobileNo = null;
	@Column(length = 255)
	private String email = null;
	@Column(length = 255)
	private String city = null;
	@Column(length = 255)
	private String state = null;
	@Column(length = 255)
	private String gymRegistered = NOTREGISTERED;
	@Column(length = 255)
	private String aboutUs = NOTAVAILABLE;
	@Column(length = 255)
	private String category1 = null;
	@Column(length = 255)
	private String category2 = null;
	@Column(length = 255)
	private String category3 = null;
	@Column(length = 255)
	private String category4 = null;
	@Column(length = 255)
	private String category5 = null;
	@Column(length = 255)
	private String location = null;
	

	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCategory1() {
		return category1;
	}
	public void setCategory1(String category1) {
		this.category1 = category1;
	}
	public String getCategory2() {
		return category2;
	}
	public void setCategory2(String category2) {
		this.category2 = category2;
	}
	public String getCategory3() {
		return category3;
	}
	public void setCategory3(String category3) {
		this.category3 = category3;
	}
	public String getCategory4() {
		return category4;
	}
	public void setCategory4(String category4) {
		this.category4 = category4;
	}
	public String getCategory5() {
		return category5;
	}
	public void setCategory5(String category5) {
		this.category5 = category5;
	}
	public String getAboutUs() {
		return aboutUs;
	}
	public void setAboutUs(String aboutUs) {
		this.aboutUs = aboutUs;
	}
	public String getGymRegistered() {
		return gymRegistered;
	}
	public void setGymRegistered(String gymRegistered) {
		this.gymRegistered = gymRegistered;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGymName() {
		return gymName;
	}
	public void setGymName(String gymName) {
		this.gymName = gymName;
	}
	/*public void setGymRegistered(String gymRegistered) {
		this.gymRegistered = gymRegistered;
	}*/
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getName() {
		return gymName;
	}
	public void setName(String name) {
		this.gymName = name;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	/*public String getGymRegistered() {
		return gymRegistered;
	}*/
	@Override
	public String getKey() {
		return id +"";
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return gymName +"";
	}
	
	
}
