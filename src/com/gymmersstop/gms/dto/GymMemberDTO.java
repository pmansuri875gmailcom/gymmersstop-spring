package com.gymmersstop.gms.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.UserDTO;
@Entity
@Table(name ="gms_gym_member")
public class GymMemberDTO extends UserDTO{
	@Column
	private long userId = 0;
	@Column(length = 255)
	private String session = null;
	@Column
	private Date joining = null;
	@Column(length = 255)
	private String goal = null;
	@Column
	private double height = 0;
	@Column
	private double weight = 0;
	@Column
	private long gymId = 0;
	
	public void setGymId(long gymId) {
		this.gymId = gymId;
	}
	public long getGymId() {
		return gymId;
	}
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public Date getJoining() {
		return joining;
	}

	public void setJoining(Date joining) {
		this.joining = joining;
	}

	public String getGoal() {
		return goal;
	}

	public void setGoal(String goal) {
		this.goal = goal;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}


	@Override
	public String getKey() {
		return id + "";
	}

	@Override
	public String getValue() {
		//We have to think about it.
		return null;
	}
	
	
	

}
