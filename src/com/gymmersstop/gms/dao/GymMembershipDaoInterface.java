package com.gymmersstop.gms.dao;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dto.GymMemberDTO;
import com.gymmersstop.gms.dto.GymMembershipDTO;

public interface GymMembershipDaoInterface {
	public long add(GymMembershipDTO dto) throws ApplicationException;

	public void delete(GymMembershipDTO dto) throws ApplicationException;

	public long update(GymMembershipDTO dto) throws ApplicationException;

	public GymMembershipDTO findByPk(long id) throws ApplicationException, NoRecordFoundException;

	public GymMembershipDTO findBySession(String session) throws ApplicationException;

	public List search(GymMembershipDTO dto, int pageNo, int PageSize) throws ApplicationException;

	public List search(GymMembershipDTO dto) throws ApplicationException;


}
