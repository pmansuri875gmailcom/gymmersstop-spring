package com.gymmersstop.gms.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymTimingDTO;
import com.gymmersstop.gms.dto.GymGalleryDTO;
import com.gymmersstop.util.DataValidator;

@Repository("gymGalleryDao")
public class GymGalleryDaoImplementation implements GymGalleryDaoInterface {
	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(GymGalleryDTO dto) throws ApplicationException {
		long pk = (long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(GymGalleryDTO dto) throws ApplicationException {
		ht.delete(dto);
	}

	@Override
	public long update(GymGalleryDTO dto) throws ApplicationException {
		ht.update(dto);
		return dto.getId();
	}

	@Override
	public GymGalleryDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		GymGalleryDTO dto = (GymGalleryDTO) ht.get(GymGalleryDTO.class, id);
		return dto;
	}

	@Override
	public List findByGymId(long id) throws ApplicationException, NoRecordFoundException {
		DetachedCriteria dc = DetachedCriteria.forClass(GymGalleryDTO.class);
		List list = (List) dc.add(Restrictions.eq("gymid", id));
		return list;
	}

	@Override
	public List<GymGalleryDTO> search(GymGalleryDTO dto, int pageNo, int pageSize) throws ApplicationException {
		List list = null;
		DetachedCriteria dc = DetachedCriteria.forClass(GymGalleryDTO.class);
		if (dto.getId() > 0) {
			dc.add(Restrictions.eq("id", dto.getId()));
		}
		if (dto.getGymId() > 0) {
			dc.add(Restrictions.eq("gymId", dto.getGymId()));
		}
		if (DataValidator.isNotNull(dto.getImageName())) {
			dc.add(Restrictions.eq("imageName", dto.getImageName()));
		}

		// if page size is greater than zero then apply pagination
		if (pageNo > 0) {
			// Calculate start record index
			pageNo = (pageNo - 1) * pageSize;
		}
		list = ht.findByCriteria(dc,pageNo,pageSize);
		return list;
	}

	@Override
	public List<GymGalleryDTO> search(GymGalleryDTO dto) throws ApplicationException {
		return search(dto, 0, 0);
	}

}
