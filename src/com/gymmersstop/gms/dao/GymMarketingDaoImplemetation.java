package com.gymmersstop.gms.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gms.dto.GymMarketingDTO;
import com.gymmersstop.util.DataValidator;

@Repository("gymMarketingDao")
public class GymMarketingDaoImplemetation implements GymMarketingDaoInterface {

	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(GymMarketingDTO dto) throws ApplicationException {
		Long pk = (Long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(GymMarketingDTO dto) throws ApplicationException {
		ht.delete(dto);
	}

	@Override
	public void update(GymMarketingDTO dto) throws ApplicationException {
		ht.update(dto);

	}

	@Override
	public GymMarketingDTO findByPk(long id) throws ApplicationException {
		GymMarketingDTO dto = (GymMarketingDTO) ht.get(GymMarketingDTO.class, id);
		return dto;
	}

	@Override
	public List search(GymMarketingDTO dto, int pageNo, int pageSize) throws ApplicationException {
		List list = null;
		DetachedCriteria dc = DetachedCriteria.forClass(GymMarketingDTO.class);
		if (dto.getId() > 0) {
			dc.add(Restrictions.eq("id", dto.getId()));
		}
		if (DataValidator.isNotNull(dto.getName())) {
			dc.add(Restrictions.like("name", dto.getName()+"%"));
		}
		if (DataValidator.isNotNull(dto.getReference())) {
			dc.add(Restrictions.like("refrence", dto.getReference()));
		}

		// if page size is greater than zero then apply pagination
		if (pageNo > 0) {
			// Calculate start record index
			pageNo = (pageNo - 1) * pageSize;
		}
		list = ht.findByCriteria(dc, pageNo, pageSize);
		return list;
	}

	@Override
	public List search(GymMarketingDTO dto) throws ApplicationException {
		return search(dto, 0, 0);
	}

}
