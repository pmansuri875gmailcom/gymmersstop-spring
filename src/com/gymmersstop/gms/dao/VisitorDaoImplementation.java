package com.gymmersstop.gms.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gms.dto.VisitorDTO;
import com.gymmersstop.util.DataValidator;

@Repository("visitorDao")
public class VisitorDaoImplementation implements VisitorDaoInterface {

	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(VisitorDTO dto) throws ApplicationException {
		return (long) ht.save(dto);
	}

	@Override
	public void update(VisitorDTO dto) throws ApplicationException {
		ht.update(dto);
	}

	@Override
	public void delete(VisitorDTO dto) throws ApplicationException {
		ht.delete(dto);
	}

	@Override
	public VisitorDTO findByPk(long pk) throws ApplicationException {
		return (VisitorDTO) ht.get(VisitorDTO.class, pk);
	}

	@Override
	public List search(VisitorDTO dto, int pageNo, int pageSize) throws ApplicationException {
		DetachedCriteria dc = DetachedCriteria.forClass(VisitorDTO.class);
		List list = null;

		if (dto.getId() > 0) {
			dc.add(Restrictions.eq("id", dto.getId()));
		}
		if (dto.getGymId() > 0) {
			dc.add(Restrictions.eq("gymId", dto.getGymId()));
		}
		if (DataValidator.isNotNull(dto.getName())) {
			dc.add(Restrictions.like("name", dto.getName()));
		}
		if (DataValidator.isNotNull(dto.getGender())) {
			dc.add(Restrictions.like("gender", dto.getGender()));
		}
		if (DataValidator.isNotNull(dto.getAddress())) {
			dc.add(Restrictions.like("address", dto.getAddress()));
		}
		if (DataValidator.isNotNull(dto.getMobileNo())) {
			dc.add(Restrictions.like("mobileNo", dto.getMobileNo()));
		}
		if (DataValidator.isNotNull(dto.getEmail())) {
			dc.add(Restrictions.like("email", dto.getEmail()));
		}
		if (DataValidator.isNotNull(dto.getGoal())) {
			dc.add(Restrictions.like("goal", dto.getGoal()));
		}

		// if page size is greater than zero then apply pagination
		if (pageNo > 0) {
			// Calculate start record index
			pageNo = (pageNo - 1) * pageSize;

		}
		list = ht.findByCriteria(dc, pageNo, pageSize);
		return list;
	}

	@Override
	public List search(VisitorDTO dto) throws ApplicationException {
		return search(dto, 0, 0);
	}

}
