package com.gymmersstop.gms.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.common.dao.AppRole;
import com.gymmersstop.common.dao.UserDAOImplementation;
import com.gymmersstop.common.dto.UserDTO;
import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gms.dto.GymMemberDTO;
import com.gymmersstop.util.DataValidator;
import com.gymmersstop.util.EmailBuilder;
import com.gymmersstop.util.EmailMessage;
import com.gymmersstop.util.EmailUtility;

@Repository("gymMemeberDao")
public class GymMemeberDaoImplementation implements GymMemeberDaoInterface {

	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(GymMemberDTO dto) throws ApplicationException {
		long pk = (long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(GymMemberDTO dto) throws ApplicationException {
		ht.delete(dto);
	}

	@Override
	public void update(GymMemberDTO dto) throws ApplicationException {
		ht.update(dto);

	}

	@Override
	public GymMemberDTO findByPk(long id) throws ApplicationException {
		GymMemberDTO dto = (GymMemberDTO) ht.get(GymMemberDTO.class, id);

		return dto;
	}

	@Override
	public GymMemberDTO findByUserId(long id) throws ApplicationException {
		DetachedCriteria dc = DetachedCriteria.forClass(GymMemberDTO.class);
		dc.add(Restrictions.eq("userId", id));
		List list = ht.findByCriteria(dc);
		GymMemberDTO dto = (GymMemberDTO) list.get(0);
		return dto;
	}

	@Override
	public List search(GymMemberDTO dto, int pageNo, int pageSize) throws ApplicationException {
		List list = null;
		DetachedCriteria dc = DetachedCriteria.forClass(GymMemberDTO.class);

		if (dto.getId() > 0) {
			dc.add(Restrictions.eq("id", dto.getId()));

		}
		if (dto.getGymId() > 0) {
			dc.add(Restrictions.eq("gymId", dto.getGymId()));

		}
		if (DataValidator.isNotNull(dto.getName())) {
			dc.add(Restrictions.eq("name", dto.getName() + "%"));
		}
		if (DataValidator.isNotNull(dto.getSession())) {
			dc.add(Restrictions.eq("session", dto.getSession() + "%"));
		}
		if (DataValidator.isNotNull(dto.getGoal())) {
			dc.add(Restrictions.eq("goal", dto.getGoal()));
		}
		if (dto.getRoleId() == AppRole.GYMMEMBER) {
			dc.add(Restrictions.eq("roleId", dto.getRoleId()));
		}
		// if page size is greater than zero then apply pagination
		if (pageNo > 0) {
			// Calculate start record index
			pageNo = (pageNo - 1) * pageSize;

		}
		list = ht.findByCriteria(dc, pageNo, pageSize);

		return list;
	}

	@Override
	public List search(GymMemberDTO dto) throws ApplicationException {
		return search(dto, 0, 0);
	}

	@Override
	public long registerGymMember(GymMemberDTO dto) throws ApplicationException {
		UserDAOImplementation dao = new UserDAOImplementation();
		long pk = dao.add(dto);
		
		if(pk>0){
			
			dto.setUserId(pk);
			add(dto);
		}HashMap<String, String> map = new HashMap<String, String>();
		// map.put("gymName", );
		map.put("login", dto.getEmail());
		map.put("password", dto.getPassword());

		String message = EmailBuilder.getGymMemberRegistrationMessage(map);
		EmailMessage msg = new EmailMessage();

		msg.setTo(dto.getEmail());
		msg.setSubject("Registration is successful for Gymmers Stop");
		msg.setMessage(message);
		msg.setMessageType(EmailMessage.HTML_MSG);

		//EmailUtility.sendMail(msg);
		return pk;	
		
	}

}
