package com.gymmersstop.gms.dao;

import java.util.List;

import org.dom4j.VisitorSupport;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gms.dto.VisitorDTO;

public interface VisitorDaoInterface {

	public long add(VisitorDTO dto)throws ApplicationException;
	public void update(VisitorDTO dto)throws ApplicationException;
	public void delete(VisitorDTO dto)throws ApplicationException;
	public VisitorDTO findByPk(long pk)throws ApplicationException;
	public List search(VisitorDTO dto , int pageNo,int pageSize)throws ApplicationException;
	public List search(VisitorDTO dto )throws ApplicationException;
	
}
