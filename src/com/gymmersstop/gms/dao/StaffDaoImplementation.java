package com.gymmersstop.gms.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.common.dao.UserDAOImplementation;
import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dto.StaffDTO;
import com.gymmersstop.util.DataValidator;
import com.gymmersstop.util.EmailBuilder;
import com.gymmersstop.util.EmailMessage;
import com.gymmersstop.util.EmailUtility;

@Repository("staffDao")
public class StaffDaoImplementation implements StaffDaoInterface {

	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(StaffDTO dto) throws ApplicationException {
		long pk = (long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(StaffDTO dto) throws ApplicationException {

		ht.delete(dto);
	}

	@Override
	public void update(StaffDTO dto) throws ApplicationException {
		ht.update(dto);
	}

	@Override
	public StaffDTO findByPk(long pk) throws ApplicationException, NoRecordFoundException {

		StaffDTO dto = (StaffDTO) ht.get(StaffDTO.class, pk);
		return dto;
	}

	@Override
	public StaffDTO findByUserId(long userId) throws ApplicationException, NoRecordFoundException {
		DetachedCriteria dc = DetachedCriteria.forClass(StaffDTO.class);
		dc.add(Restrictions.eq("userId", userId));
		StaffDTO dto = (StaffDTO) ht.findByCriteria(dc);
		return dto;
	}

	@Override
	public List search(StaffDTO dto, int pageNo, int pageSize) throws ApplicationException {
		DetachedCriteria dc = DetachedCriteria.forClass(StaffDTO.class);
		List list = null;

		if (dto.getGymId() > 0) {
			dc.add(Restrictions.eq("gymId", dto.getGymId()));
		}

		if (DataValidator.isNotNull(dto.getName())) {
			dc.add(Restrictions.eq("name", dto.getName() + "%"));
		}

		// if page size is greater than zero then apply pagination
		if (pageSize > 0) {
			// Calculate start record index
			pageNo = (pageNo - 1) * pageSize;
		}
		list = ht.findByCriteria(dc, pageNo, pageSize);

		return list;
	}

	@Override
	public List search(StaffDTO dto) throws ApplicationException {
		return search(dto, 0, 0);
	}

	@Override
	public long registerStaff(StaffDTO dto) throws ApplicationException {
		UserDAOImplementation udi = new UserDAOImplementation();
		long pk = udi.add(dto);
		System.out.println("registerUser Started : 1");
		if (pk > 0) {
			dto.setUserId(pk);
			StaffDaoImplementation sdi = new StaffDaoImplementation();
			sdi.add(dto);
		}
		HashMap<String, String> map = new HashMap<String, String>();
		// map.put("gymName", );
		map.put("login", dto.getEmail());
		map.put("password", dto.getPassword());

		String message = EmailBuilder.getGymStaffRegistrationMessage(map);
		EmailMessage msg = new EmailMessage();

		msg.setTo(dto.getEmail());
		msg.setSubject("Registration is successful for Gymmers Stop");
		msg.setMessage(message);
		msg.setMessageType(EmailMessage.HTML_MSG);

		EmailUtility.sendMail(msg);
		return pk;
	}

}
