package com.gymmersstop.gms.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.aop.framework.DefaultAdvisorChainFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.common.dao.AppRole;
import com.gymmersstop.common.dao.UserDAOImplementation;
import com.gymmersstop.common.dto.UserDTO;
import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dao.GymFacilitiesDaoImplementation;
import com.gymmersstop.gd.dao.GymTimingDaoImplementation;
import com.gymmersstop.gd.dto.GymFacilitiesDTO;
import com.gymmersstop.gd.dto.GymTimingDTO;
import com.gymmersstop.gms.dto.GymDTO;
import com.gymmersstop.util.DataValidator;
import com.gymmersstop.util.EmailBuilder;
import com.gymmersstop.util.EmailMessage;
import com.gymmersstop.util.EmailUtility;
import com.gymmersstop.util.GenrateRandomPassword;

@Repository("gymDao")
public class GymDaoImplementation implements GymDaoInterface {

	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(GymDTO dto) throws ApplicationException {
		long pk = (long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(GymDTO dto) throws ApplicationException {

		ht.delete(dto);

	}

	@Override
	public void update(GymDTO dto) throws ApplicationException {
		ht.update(dto);
	}

	@Override
	public GymDTO findByPk(long pk) throws ApplicationException, NoRecordFoundException {
		GymDTO dto = (GymDTO) ht.get(GymDTO.class, pk);
		return dto;
	}

	@Override
	public GymDTO findByLogin(String email) throws ApplicationException, NoRecordFoundException {
		DetachedCriteria dc = DetachedCriteria.forClass(GymDTO.class);
		dc.add(Restrictions.eq("email", email));
		GymDTO dto = (GymDTO) ht.findByCriteria(dc);
		return dto;
	}

	@Override
	public List search(GymDTO dto, int pageNo, int pageSize) throws ApplicationException {
		List list = null;
		DetachedCriteria dc = DetachedCriteria.forClass(GymDTO.class);

		if (dto.getId() > 0) {
			dc.add(Restrictions.eq("id", dto.getId()));
		}

		if (DataValidator.isNotNull(dto.getName())) {
			dc.add(Restrictions.like("name", dto.getName() + "%"));
		}
		if (DataValidator.isNotNull(dto.getCity())) {
			dc.add(Restrictions.like("city", dto.getCity() + "%"));
		}
		if (DataValidator.isNotNull(dto.getCategory1())) {
			dc.add(Restrictions.like("category1", dto.getCategory1() + "%"));
		}
		if (DataValidator.isNotNull(dto.getCategory2())) {
			dc.add(Restrictions.like("category2", dto.getCategory2() + "%"));
		}
		if (DataValidator.isNotNull(dto.getCategory3())) {
			dc.add(Restrictions.like("category3", dto.getCategory3() + "%"));
		}
		if (DataValidator.isNotNull(dto.getCategory4())) {
			dc.add(Restrictions.like("category4", dto.getCategory4() + "%"));
		}
		if (DataValidator.isNotNull(dto.getCategory5())) {
			dc.add(Restrictions.like("category5", dto.getCategory5() + "%"));
		}
		if (DataValidator.isNotNull(dto.getAddress())) {
			dc.add(Restrictions.like("address", dto.getAddress() + "%"));
		}
		if (DataValidator.isNotNull(dto.getOwnerName())) {
			dc.add(Restrictions.like("ownername", dto.getOwnerName() + "%"));
		}
		if (DataValidator.isNotNull(dto.getMobileNo())) {
			dc.add(Restrictions.like("mobileNo", dto.getMobileNo() + "%"));
		}
		if (DataValidator.isNotNull(dto.getEmail())) {
			dc.add(Restrictions.like("email", dto.getEmail() + "%"));
		}
		if (DataValidator.isNotNull(dto.getLocation())) {

			dc.add(Restrictions.like("location", dto.getLocation() + "%"));
		}

		// if page size is greater than zero then apply pagination

		if (pageNo > 0) {

			// Calculate start record index
			pageNo = (pageNo - 1) * pageSize;

		}
		list = ht.findByCriteria(dc, pageNo, pageSize);

		return list;
	}

	@Override
	public List search(GymDTO dto) throws ApplicationException {
		return search(dto, 0, 0);
	}

	@Override
	public GymDTO authenticate(String email, String password) throws ApplicationException {
		DetachedCriteria dc = DetachedCriteria.forClass(GymDTO.class);
		dc.add(Restrictions.eq("email", email));
		dc.add(Restrictions.eq("password", password));
		List list = ht.findByCriteria(dc);
		GymDTO dto = (GymDTO) list.get(0);
		return dto;
	}

	@Override
	public long registerGym(GymDTO dto) throws ApplicationException {
		long pk = add(dto);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("email", dto.getEmail());

		String message = EmailBuilder.getGymRegistrationMessage(map);
		EmailMessage msg = new EmailMessage();

		msg.setTo(dto.getEmail());
		msg.setSubject("Registration is successful for Gymmers Stop");
		msg.setMessage(message);
		msg.setMessageType(EmailMessage.HTML_MSG);

		EmailUtility.sendMail(msg);
		return pk;
	}

	@Override
	public long registerGymListing(GymDTO dto) throws ApplicationException {
		long pk = add(dto);
		GymTimingDTO tdto = new GymTimingDTO();
		tdto.setGymId(pk);
		
		GymTimingDaoImplementation gtdi = new GymTimingDaoImplementation();
		gtdi.add(tdto);

		GymFacilitiesDTO fbean = new GymFacilitiesDTO();
		fbean.setGymId(pk);
		
		GymFacilitiesDaoImplementation gfdi = new GymFacilitiesDaoImplementation();
		long a = gfdi.add(fbean);

		if (pk > 0) {
			UserDTO ubean = new UserDTO();
			ubean.setRoleId(AppRole.GDGYMADMIN);
			ubean.setGymId(pk);
			ubean.setEmail(dto.getEmail());
			ubean.setPassword(GenrateRandomPassword.generateRandomPassword(8));
			GymTimingDTO timeBean = new GymTimingDTO();
			timeBean.setGymId(pk);
			GymTimingDaoImplementation gtdi1 = new GymTimingDaoImplementation();
			gtdi1.add(timeBean);
			GymFacilitiesDTO gfd = new GymFacilitiesDTO();
			gfd.setGymId(pk);
			GymFacilitiesDaoImplementation gfdi1 = new GymFacilitiesDaoImplementation();
			gfdi1.add(gfd);
			UserDAOImplementation udi = new UserDAOImplementation();
			pk = udi.add(ubean);

		}
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("email", dto.getEmail());

		String message = EmailBuilder.getGymRegistrationMessage(map);
		EmailMessage msg = new EmailMessage();

		msg.setTo(dto.getEmail());
		msg.setSubject(dto.getName() + " is successfully list on Gymmers Stop");
		msg.setMessage(message);
		msg.setMessageType(EmailMessage.HTML_MSG);

		EmailUtility.sendMail(msg);
		return pk;
	}

}
