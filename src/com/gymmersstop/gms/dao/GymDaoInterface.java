package com.gymmersstop.gms.dao;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dto.GymDTO;

public interface GymDaoInterface {

	public long add(GymDTO dto)throws ApplicationException;
	public void delete(GymDTO dto)throws ApplicationException;
	public void update(GymDTO dto)throws ApplicationException;
	public GymDTO findByPk(long pk)throws ApplicationException,NoRecordFoundException;
	public GymDTO findByLogin(String email)throws ApplicationException,NoRecordFoundException;
	public List search(GymDTO dto,int pageNo,int pageSize) throws ApplicationException;
	public List search(GymDTO dto)throws ApplicationException;
	public GymDTO authenticate(String email,String password)throws ApplicationException;
	public long registerGym(GymDTO dto)throws ApplicationException;
	public long registerGymListing(GymDTO dto)throws ApplicationException;
	
	
}
