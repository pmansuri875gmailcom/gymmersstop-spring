package com.gymmersstop.gms.dao;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymTimingDTO;
import com.gymmersstop.gms.dto.GymGalleryDTO;

public interface GymGalleryDaoInterface {
	public long add(GymGalleryDTO dto)throws ApplicationException;
	public void delete(GymGalleryDTO dto) throws ApplicationException;
	public long update(GymGalleryDTO dto)throws ApplicationException;
	public GymGalleryDTO findByPk(long id)throws ApplicationException, NoRecordFoundException;
	public List findByGymId(long id)throws ApplicationException, NoRecordFoundException;
	public List<GymGalleryDTO> search(GymGalleryDTO dto,int pageNo,int pageSize)throws ApplicationException;
	public List<GymGalleryDTO> search(GymGalleryDTO dto)throws ApplicationException;
	
}
