package com.gymmersstop.gms.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dto.GymLogoDTO;

@Repository("gymLogoDao")
public class GymLogoDaoImplementation implements GymLogoDaoInterface {

	@Autowired
	private HibernateTemplate ht;
	
	@Override
	public long add(GymLogoDTO dto) throws ApplicationException {
		long pk = (long)ht.save(dto);
		return pk;
	}

	@Override
	public void delete(GymLogoDTO dto) throws ApplicationException {
		ht.delete(dto);
		
	}

	@Override
	public void update(GymLogoDTO dto) throws ApplicationException {

		ht.update(dto);
	}

	@Override
	public GymLogoDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		GymLogoDTO dto = (GymLogoDTO) ht.get(GymLogoDTO.class, id);
		return dto;
	}

	@Override
	public GymLogoDTO findByGymId(long gymId) throws ApplicationException {
		DetachedCriteria dc = DetachedCriteria.forClass(GymLogoDTO.class);
		dc.add(Restrictions.eq("gymId", gymId));
		List list = ht.findByCriteria(dc);
		GymLogoDTO dto = (GymLogoDTO) list.get(0);
				
		return dto;
	}

}
