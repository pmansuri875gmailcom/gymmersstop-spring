package com.gymmersstop.gms.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.common.dao.AppRole;
import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dto.GymMemberDTO;
import com.gymmersstop.gms.dto.GymMembershipDTO;
import com.gymmersstop.util.DataValidator;
import com.gymmersstop.util.EmailBuilder;
import com.gymmersstop.util.EmailMessage;
import com.gymmersstop.util.EmailUtility;

@Repository("gymMemberDao")
public class GymMembershipDaoImplemetation implements GymMembershipDaoInterface {

	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(GymMembershipDTO dto) throws ApplicationException {
		long pk = (long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(GymMembershipDTO dto) throws ApplicationException {
		ht.delete(dto);
	}

	@Override
	public long update(GymMembershipDTO dto) throws ApplicationException {
		ht.update(dto);
		return dto.getId();
	}

	@Override
	public GymMembershipDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		GymMembershipDTO dto = (GymMembershipDTO) ht.get(GymMemberDTO.class, id);
		return dto;
	}

	@Override
	public GymMembershipDTO findBySession(String session) throws ApplicationException {
		DetachedCriteria dc = DetachedCriteria.forClass(GymMemberDTO.class);
		dc.add(Restrictions.eq("session", session));
		GymMembershipDTO dto = (GymMembershipDTO) ht.findByCriteria(dc);
		return dto;
	}

	@Override
	public List search(GymMembershipDTO dto, int pageNo, int PageSize) throws ApplicationException {
		List list = null;
		DetachedCriteria dc = DetachedCriteria.forClass(GymMemberDTO.class);
		if (dto.getId() > 0) {
			dc.add(Restrictions.eq("id", dto.getId()));
		}
		if (DataValidator.isNotNull(dto.getSession())) {
		dc.add(Restrictions.eq("session", dto.getSession()));	
		}
		// if page size is greater than zero then apply pagination
		if (pageNo > 0) {
			// Calculate start record index
			pageNo = (pageNo - 1) * PageSize;


		}
		list = ht.findByCriteria(dc, pageNo, PageSize);
		
		return list;
	}

	@Override
	public List search(GymMembershipDTO dto) throws ApplicationException {
		// TODO Auto-generated method stub
		return search(dto, 0, 0);
	}

	

	
		
}
