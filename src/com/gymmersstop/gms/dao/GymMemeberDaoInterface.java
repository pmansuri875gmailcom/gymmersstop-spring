package com.gymmersstop.gms.dao;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gms.dto.GymMemberDTO;

public interface GymMemeberDaoInterface {

	public long add(GymMemberDTO dto) throws ApplicationException;

	public void delete(GymMemberDTO dto) throws ApplicationException;

	public void update(GymMemberDTO dto) throws ApplicationException;

	public GymMemberDTO findByPk(long id) throws ApplicationException;

	public GymMemberDTO findByUserId(long id) throws ApplicationException;

	public List search(GymMemberDTO dto, int pageNo, int pageSize) throws ApplicationException;

	public List search(GymMemberDTO dto) throws ApplicationException;

	public long registerGymMember(GymMemberDTO dto) throws ApplicationException;

}
