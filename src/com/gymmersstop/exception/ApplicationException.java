package com.gymmersstop.exception;

public class ApplicationException extends RuntimeException{

	/**
	 * ApplicationException is propogated from Service Classes when a business logic exception occurred.
	 * @param msg
	 * @author Tofique Ahmed Khan
	 * @version 1.0
	 */
	
	Exception rootException = null;
	
	public ApplicationException(String msg) {
		super(msg);
	}
	
	public ApplicationException(Exception e){
		super(e.getMessage());
		rootException = e;
	}
}
