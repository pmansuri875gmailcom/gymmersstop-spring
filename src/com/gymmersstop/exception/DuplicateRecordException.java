package com.gymmersstop.exception;

public class DuplicateRecordException extends RuntimeException {

	public DuplicateRecordException(String msg) {
		super(msg);
	}
}
