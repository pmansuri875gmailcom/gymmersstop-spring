package com.gymmersstop.gs.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.BaseDTO;
@Entity
@Table(name = "gs_team" )
public class TeamDTO extends BaseDTO {
	@Column(length = 255)
	private String name = null;
	@Column(length = 255)
	private String email = null;
	@Column(length = 255)
	private String password = null;
	private String confirmPassword = null;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	@Override
	public String getKey() {
		return null;
	}
	@Override
	public String getValue() {
		return null;
	}
	
	
}
