package com.gymmersstop.gs.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gs.dto.TeamDTO;
import com.sun.mail.handlers.text_html;
@Repository("teamDao")
public class TeamDaoImplementation implements TeamDaoInterface {

	@Autowired
	private HibernateTemplate ht;
	
	@Override
	public long add(TeamDTO dto) throws ApplicationException {
		return (long)ht.save(dto);
	}

	@Override
	public void update(TeamDTO dto) throws ApplicationException {

		ht.update(dto);
	}

	@Override
	public void delete(TeamDTO dto) throws ApplicationException {
		ht.delete(dto);
	}

	@Override
	public TeamDTO findByPk(long pk) throws ApplicationException {
		return (TeamDTO)ht.get(TeamDTO.class, pk);
	}

}
