package com.gymmersstop.gs.dao;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gs.dto.TeamDTO;

public interface TeamDaoInterface {

	public long add(TeamDTO dto)throws ApplicationException;
	public void update(TeamDTO dto)throws ApplicationException;
	public void delete(TeamDTO dto)throws ApplicationException;
	public TeamDTO findByPk(long pk)throws ApplicationException;
}
