package com.gymmersstop.gs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.gs.dao.TeamDaoInterface;
import com.gymmersstop.gs.dto.TeamDTO;

public class TeamServiceSpringImplementation implements TeamServiceInterface {

	@Autowired
	private TeamDaoInterface dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public long add(TeamDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(TeamDTO dto) throws ApplicationException {

		dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(TeamDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public TeamDTO findByPk(long pk) throws ApplicationException {
		return dao.findByPk(pk);
	}

}
