package com.gymmersstop.gd.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymMembershipPackageDTO;
import com.gymmersstop.util.DataValidator;

@Repository("gymMembPackDao")
public class GymMembPackDaoImpelementation implements GymMembPackDaoInterface {
	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(GymMembershipPackageDTO dto) throws ApplicationException {
		long pk = (long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(GymMembershipPackageDTO dto) throws ApplicationException {
		ht.delete(dto);
	}

	@Override
	public long update(GymMembershipPackageDTO dto) throws ApplicationException {
		ht.update(dto);
		return dto.getId();
	}

	@Override
	public GymMembershipPackageDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		GymMembershipPackageDTO dto = null;
		dto = (GymMembershipPackageDTO) ht.get(GymMembershipPackageDTO.class, id);
		return dto;
	}

	@Override
	public List<GymMembershipPackageDTO> search(GymMembershipPackageDTO dto, int pageNo, int pageSize)
			throws ApplicationException {
		List list = null;
		DetachedCriteria dc = DetachedCriteria.forClass(GymMembershipPackageDTO.class);
		if (dto.getId() > 0) {
			dc.add(Restrictions.eq("id", dto.getId()));
		}
		if (DataValidator.isNotNull(dto.getPackageName())) {
			dc.add(Restrictions.like("packageName", dto.getPackageName() + "%"));
		}
		if (dto.getDays() > 0) {
			dc.add(Restrictions.like("days", dto.getDays() + "%"));
		}
		// if page size is greater than zero apply pagination
		if (pageNo > 0) {
			// calculate record index
			pageNo = (pageNo - 1) * pageSize;
		}
		list = ht.findByCriteria(dc, pageNo, pageSize);
		return list;
	}

	@Override
	public List<GymMembershipPackageDTO> search(GymMembershipPackageDTO dto) throws ApplicationException {
		return search(dto, 0, 0);
	}

}
