package com.gymmersstop.gd.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymFacilitiesDTO;
import com.gymmersstop.util.DataValidator;

@Repository("gymFacilitiesDao")
public class GymFacilitiesDaoImplementation implements GymFacilitiesDaoInterface {
	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(GymFacilitiesDTO dto) throws ApplicationException {
		long pk = (long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(GymFacilitiesDTO dto) throws ApplicationException {
		ht.delete(dto);
	}

	@Override
	public long update(GymFacilitiesDTO dto) throws ApplicationException {
		ht.update(dto);

		return dto.getId();
	}

	@Override
	public GymFacilitiesDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		GymFacilitiesDTO dto = null;
		dto = (GymFacilitiesDTO) ht.get(GymFacilitiesDTO.class, id);
		return dto;
	}

	@Override
	public GymFacilitiesDTO findByGymId(long gymId) throws ApplicationException, NoRecordFoundException {
		GymFacilitiesDTO dto = null;
		DetachedCriteria dc = DetachedCriteria.forClass(GymFacilitiesDTO.class);
		dc.add(Restrictions.eq("gymid", gymId));
		List list = ht.findByCriteria(dc);
		dto = (GymFacilitiesDTO) list.get(0);

		return dto;
	}

	@Override
	public List<GymFacilitiesDTO> search(GymFacilitiesDTO dto, int pageNo, int pageSize) throws ApplicationException {
		List list = null;
		DetachedCriteria dc = DetachedCriteria.forClass(GymFacilitiesDTO.class);
		if (dto != null) {
			if (dto.getId() > 0) {
				dc.add(Restrictions.eq("id", dto.getId()));
			}
			if (dto.getGymId() > 0) {
				dc.add(Restrictions.eq("gymId", dto.getGymId()));
			}
			if (DataValidator.isNotNull(dto.getAc())) {
				dc.add(Restrictions.like("ac", dto.getAc() + "%"));
			}
			if (DataValidator.isNotNull(dto.getParking())) {
				dc.add(Restrictions.like("parking", dto.getParking() + "%"));
			}
			if (DataValidator.isNotNull(dto.getSteam())) {
				dc.add(Restrictions.like("steam", dto.getSteam() + "%"));
			}
			if (DataValidator.isNotNull(dto.getMinralWater())) {
				dc.add(Restrictions.like("minralWater", dto.getMinralWater() + "%"));
			}
			if (DataValidator.isNotNull(dto.getDietitian())) {
				dc.add(Restrictions.like("dietition", dto.getDietitian() + "%"));
			}
			if (DataValidator.isNotNull(dto.getLocker())) {
				dc.add(Restrictions.like("locker", dto.getLocker() + "%"));
			}

			// if page size is greater than zero then apply pagination
			if (pageNo > 0) {
				// Calculate start record index
				pageNo = (pageNo - 1) * pageSize;
			}
			list = ht.findByCriteria(dc, pageNo, pageSize);
		}

		return list;
	}

	@Override
	public List<GymFacilitiesDTO> search(GymFacilitiesDTO dto) throws ApplicationException {
		return search(dto, 0, 0);
	}

}
