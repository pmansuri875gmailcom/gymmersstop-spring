package com.gymmersstop.gd.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymReviewDTO;
import com.gymmersstop.util.DataValidator;

@Repository("gymReview")
public class GymReviewDaoImplementation implements GymReviewDaoInterface {
	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(GymReviewDTO dto) throws ApplicationException {
		long pk = (long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(GymReviewDTO dto) throws ApplicationException {
		ht.delete(dto);
	}

	@Override
	public long update(GymReviewDTO dto) throws ApplicationException {
		ht.update(dto);
		return dto.getId();
	}

	@Override
	public GymReviewDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		GymReviewDTO dto = null;
		dto = (GymReviewDTO) ht.get(GymReviewDTO.class, id);
		return dto;
	}

	@Override
	public List<GymReviewDTO> search(GymReviewDTO dto, int pageNo, int pageSize) throws ApplicationException {
		List list = null;
		DetachedCriteria dc = DetachedCriteria.forClass(GymReviewDTO.class);
		if(dto.getId()>0){
			dc.add(Restrictions.eq("id", dto.getId()));
		}
		if(DataValidator.isNotNull(dto.getMessage())){
			dc.add(Restrictions.eq("message", dto.getMessage()));
		}
		if(dto.getStars()>0){
		dc.add(Restrictions.eq("stars", dto.getStars()));
		}
		//if page size is greater than zero apply pagiation
		if(pageNo>0){
			//calculate record index
			pageNo = (pageNo-1)*pageSize;
		}
		list = ht.findByCriteria(dc, pageNo, pageSize);
		return list;
	}

	@Override
	public List<GymReviewDTO> search(GymReviewDTO dto) throws ApplicationException {
		return search(dto, 0, 0);
	}

}
