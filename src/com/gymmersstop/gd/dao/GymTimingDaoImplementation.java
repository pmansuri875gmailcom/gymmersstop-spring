package com.gymmersstop.gd.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymTimingDTO;

@Repository("gymTimingDao")
public class GymTimingDaoImplementation implements GymTimingDaoInterface {
	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(GymTimingDTO dto) throws ApplicationException {
		long pk = (long) ht.save(dto);
		
		return pk;
	}

	@Override
	public void delete(GymTimingDTO dto) throws ApplicationException {

		ht.delete(dto);
	}

	@Override
	public long update(GymTimingDTO dto) throws ApplicationException {
		ht.update(dto);
		return dto.getId();
	}

	@Override
	public GymTimingDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		GymTimingDTO dto = (GymTimingDTO) ht.get(GymTimingDTO.class, id);
		return dto;
	}

	@Override
	public GymTimingDTO findByGymId(long id) throws ApplicationException, NoRecordFoundException {
		GymTimingDTO dto = null;
		DetachedCriteria dc = DetachedCriteria.forClass(GymTimingDTO.class);
		dc.add(Restrictions.eq("gymId", id));
		List list = ht.findByCriteria(dc);
		dto = (GymTimingDTO) list.get(0);
		return dto;
	}

	@Override
	public List<GymTimingDTO> search(GymTimingDTO dto, int pageNo, int pageSize) throws ApplicationException {
		
		
		return null;
	}

	@Override
	public List<GymTimingDTO> search(GymTimingDTO dto) throws ApplicationException {
		return search(dto, 0, 0);
	}

}
