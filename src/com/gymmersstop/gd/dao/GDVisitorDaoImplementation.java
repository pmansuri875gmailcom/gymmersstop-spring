package com.gymmersstop.gd.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dto.VisitorDTO;
import com.gymmersstop.util.DataValidator;

import javassist.compiler.ast.Visitor;

@Repository("gdVisitorDao")
public class GDVisitorDaoImplementation implements GDVisitorDaoInterface {

	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(VisitorDTO dto) throws ApplicationException {
		long pk = (long) ht.save(dto);
		return pk;
	}

	@Override
	public void delete(VisitorDTO dto) throws ApplicationException {
		ht.delete(dto);

	}

	@Override
	public long update(VisitorDTO dto) throws ApplicationException {
		ht.update(dto);
		return dto.getId();
	}

	@Override
	public VisitorDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		VisitorDTO dto = (VisitorDTO) ht.get(VisitorDTO.class, id);
		return dto;
	}

	@Override
	public VisitorDTO findByLogin(String login) throws ApplicationException, NoRecordFoundException {
		VisitorDTO dto = null;
		DetachedCriteria dc = DetachedCriteria.forClass(VisitorDTO.class);
		dc.add(Restrictions.eq("login", login));
		List list = ht.findByCriteria(dc);
		dto = (VisitorDTO) list.get(0);
		return dto;
	}

	@Override
	public List<VisitorDTO> search(VisitorDTO dto, int pageNo, int pageSize) throws ApplicationException {
		List list = null;
		DetachedCriteria dc = DetachedCriteria.forClass(VisitorDTO.class);
		if(dto!=null){
			if (dto.getId() > 0) {
				dc.add(Restrictions.eq("id", dto.getId()));
			}
			if(DataValidator.isNotNull(dto.getName())){
				dc.add(Restrictions.like("name", dto.getName()+"%"));
			}
			if(DataValidator.isNotNull(dto.getEmail())){
				dc.add(Restrictions.like("email", dto.getEmail()+"%"));
			}
		}
		if (pageNo > 0) {
			// Calculate start record index
			pageNo = (pageNo - 1) * pageSize;
		}
		list = ht.findByCriteria(dc,pageNo, pageSize);

		return list;
	}

	@Override
	public List<VisitorDTO> search(VisitorDTO dto) throws ApplicationException {
		return search(dto,0,0);
	}

}
