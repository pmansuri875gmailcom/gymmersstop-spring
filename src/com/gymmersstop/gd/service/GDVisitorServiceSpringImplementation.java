package com.gymmersstop.gd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dao.GDVisitorDaoInterface;
import com.gymmersstop.gms.dto.VisitorDTO;

public class GDVisitorServiceSpringImplementation implements GDVisitorServiceInterface {
	@Autowired
	GDVisitorDaoInterface dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW , readOnly = false)
	public long add(VisitorDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public void delete(VisitorDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public long update(VisitorDTO dto) throws ApplicationException {
		return dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public VisitorDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		return dao.findByPk(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public VisitorDTO findByLogin(String login) throws ApplicationException, NoRecordFoundException {
		return dao.findByLogin(login);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public List<VisitorDTO> search(VisitorDTO dto, int pageNo, int pageSize) throws ApplicationException {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public List<VisitorDTO> search(VisitorDTO dto) throws ApplicationException {
		return dao.search(dto);
	}

}
