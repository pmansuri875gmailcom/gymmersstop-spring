package com.gymmersstop.gd.service;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymTimingDTO;

public interface GymTimingServiceInterface {
	public long add(GymTimingDTO dto)throws ApplicationException;
	public void delete(GymTimingDTO dto) throws ApplicationException;
	public long update(GymTimingDTO dto)throws ApplicationException;
	public GymTimingDTO findByPk(long id)throws ApplicationException, NoRecordFoundException;
	public GymTimingDTO findByGymId(long id)throws ApplicationException, NoRecordFoundException;
	public List<GymTimingDTO> search(GymTimingDTO dto,int pageNo,int pageSize)throws ApplicationException;
	public List<GymTimingDTO> search(GymTimingDTO dto)throws ApplicationException;

}
