package com.gymmersstop.gd.service;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gms.dto.VisitorDTO;

public interface GDVisitorServiceInterface {
	public long add(VisitorDTO dto)throws ApplicationException;
	public void delete(VisitorDTO dto) throws ApplicationException;
	public long update(VisitorDTO dto)throws ApplicationException;
	public VisitorDTO findByPk(long id)throws ApplicationException, NoRecordFoundException;
	public VisitorDTO findByLogin(String login)throws ApplicationException, NoRecordFoundException;
	public List<VisitorDTO> search(VisitorDTO dto,int pageNo,int pageSize)throws ApplicationException;
	public List<VisitorDTO> search(VisitorDTO dto)throws ApplicationException;
}
