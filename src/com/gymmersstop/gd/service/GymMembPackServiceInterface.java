package com.gymmersstop.gd.service;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymMembershipPackageDTO;

public interface GymMembPackServiceInterface {
	public long add(GymMembershipPackageDTO dto)throws ApplicationException;
	public void delete(GymMembershipPackageDTO dto) throws ApplicationException;
	public long update(GymMembershipPackageDTO dto)throws ApplicationException;
	public GymMembershipPackageDTO findByPk(long id)throws ApplicationException, NoRecordFoundException;
	public List<GymMembershipPackageDTO> search(GymMembershipPackageDTO dto,int pageNo,int pageSize)throws ApplicationException;
	public List<GymMembershipPackageDTO> search(GymMembershipPackageDTO dto)throws ApplicationException;
}
