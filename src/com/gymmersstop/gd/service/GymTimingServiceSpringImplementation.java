package com.gymmersstop.gd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dao.GymTimingDaoInterface;
import com.gymmersstop.gd.dto.GymTimingDTO;

public class GymTimingServiceSpringImplementation  implements GymTimingServiceInterface{

	@Autowired
	private GymTimingDaoInterface dao ;
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW , readOnly = false)
	public long add(GymTimingDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public void delete(GymTimingDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public long update(GymTimingDTO dto) throws ApplicationException {
		return dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public GymTimingDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		return dao.findByPk(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public GymTimingDTO findByGymId(long id) throws ApplicationException, NoRecordFoundException {
		return dao.findByGymId(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public List<GymTimingDTO> search(GymTimingDTO dto, int pageNo, int pageSize) throws ApplicationException {
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public List<GymTimingDTO> search(GymTimingDTO dto) throws ApplicationException {
		return null;
	}

}
