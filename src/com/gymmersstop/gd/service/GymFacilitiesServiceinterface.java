package com.gymmersstop.gd.service;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymFacilitiesDTO;

public interface GymFacilitiesServiceinterface {
	public long add(GymFacilitiesDTO dto)throws ApplicationException;
	public void delete(GymFacilitiesDTO dto) throws ApplicationException;
	public long update(GymFacilitiesDTO dto)throws ApplicationException;
	public GymFacilitiesDTO findByPk(long id)throws ApplicationException, NoRecordFoundException;
	public GymFacilitiesDTO findByGymId(long gymId)throws ApplicationException, NoRecordFoundException;
	public List<GymFacilitiesDTO> search(GymFacilitiesDTO dto,int pageNo,int pageSize)throws ApplicationException;
	public List<GymFacilitiesDTO> search(GymFacilitiesDTO dto)throws ApplicationException;
}
