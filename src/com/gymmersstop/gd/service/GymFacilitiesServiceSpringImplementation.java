package com.gymmersstop.gd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dao.GymFacilitiesDaoInterface;
import com.gymmersstop.gd.dto.GymFacilitiesDTO;

public class GymFacilitiesServiceSpringImplementation implements GymFacilitiesServiceinterface {
	@Autowired
	GymFacilitiesDaoInterface dao;
	@Override
	@Transactional(propagation  = Propagation.REQUIRES_NEW , readOnly = false)
	public long add(GymFacilitiesDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation  = Propagation.REQUIRED, readOnly = false)
	public void delete(GymFacilitiesDTO dto) throws ApplicationException {
		dao.delete(dto);
		
	}

	@Override
	@Transactional(propagation  = Propagation.REQUIRED, readOnly = false)
	public long update(GymFacilitiesDTO dto) throws ApplicationException {
		return dao.update(dto);
	}

	@Override
	@Transactional(propagation  = Propagation.REQUIRED, readOnly = false)
	public GymFacilitiesDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		return dao.findByPk(id);
	}

	@Override
	@Transactional(propagation  = Propagation.REQUIRED, readOnly = false)
	public GymFacilitiesDTO findByGymId(long gymId) throws ApplicationException, NoRecordFoundException {
		return dao.findByGymId(gymId);
	}

	@Override
	@Transactional(propagation  = Propagation.REQUIRED, readOnly = false)
	public List<GymFacilitiesDTO> search(GymFacilitiesDTO dto, int pageNo, int pageSize) throws ApplicationException {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	@Transactional(propagation  = Propagation.REQUIRED, readOnly = false)
	public List<GymFacilitiesDTO> search(GymFacilitiesDTO dto) throws ApplicationException {
		return dao.search(dto);
	}
}
