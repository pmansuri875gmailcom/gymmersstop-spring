package com.gymmersstop.gd.service;

import java.util.List;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dto.GymReviewDTO;

public interface GymReviewServiceInterface {
	public long add(GymReviewDTO dto)throws ApplicationException;
	public void delete(GymReviewDTO dto) throws ApplicationException;
	public long update(GymReviewDTO dto)throws ApplicationException;
	public GymReviewDTO findByPk(long id)throws ApplicationException, NoRecordFoundException;
	public List<GymReviewDTO> search(GymReviewDTO dto,int pageNo,int pageSize)throws ApplicationException;
	public List<GymReviewDTO> search(GymReviewDTO dto)throws ApplicationException;

}
