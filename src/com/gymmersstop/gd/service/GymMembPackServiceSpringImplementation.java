package com.gymmersstop.gd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dao.GymMembPackDaoInterface;
import com.gymmersstop.gd.dto.GymMembershipPackageDTO;

public class GymMembPackServiceSpringImplementation implements GymMembPackServiceInterface {
	@Autowired
	private GymMembPackDaoInterface dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public long add(GymMembershipPackageDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(GymMembershipPackageDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public long update(GymMembershipPackageDTO dto) throws ApplicationException {
		return dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public GymMembershipPackageDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		return dao.findByPk(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<GymMembershipPackageDTO> search(GymMembershipPackageDTO dto, int pageNo, int pageSize)
			throws ApplicationException {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<GymMembershipPackageDTO> search(GymMembershipPackageDTO dto) throws ApplicationException {
		return dao.search(dto);
	}

}
