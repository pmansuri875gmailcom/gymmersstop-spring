package com.gymmersstop.gd.service;

import java.util.List;

import org.apache.commons.io.filefilter.FalseFileFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.exception.NoRecordFoundException;
import com.gymmersstop.gd.dao.GymReviewDaoInterface;
import com.gymmersstop.gd.dto.GymReviewDTO;

public class GymReviewServiceSpringImplementation implements GymReviewServiceInterface {

	@Autowired
	private GymReviewDaoInterface dao;
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW , readOnly = false)
	public long add(GymReviewDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(GymReviewDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public long update(GymReviewDTO dto) throws ApplicationException {
		return dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public GymReviewDTO findByPk(long id) throws ApplicationException, NoRecordFoundException {
		return dao.findByPk(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public List<GymReviewDTO> search(GymReviewDTO dto, int pageNo, int pageSize) throws ApplicationException {
		return dao.search(dto, pageNo, pageSize);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public List<GymReviewDTO> search(GymReviewDTO dto) throws ApplicationException {
		return dao.search(dto);
	}

}
