package com.gymmersstop.gd.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.BaseDTO;

@Entity
@Table(name = "gd_gym_timing")
public class GymTimingDTO extends BaseDTO {

	public static final String NOT_AVALABLE = "Not Available";
	@Column(length = 255)
	private long gymId = 0;
	@Column(length = 255)
	private String morningOpeningTime = NOT_AVALABLE;
	@Column(length = 255)
	private String morningClosingTime = NOT_AVALABLE;
	@Column(length = 255)
	private String eveningOpeningTime = NOT_AVALABLE;
	@Column(length = 255)
	private String eveningClosingTime = NOT_AVALABLE;
	
	
	
	public String getMorningOpeningTime() {
		return morningOpeningTime;
	}

	public void setMorningOpeningTime(String morningOpeningTime) {
		this.morningOpeningTime = morningOpeningTime;
	}

	public String getMorningClosingTime() {
		return morningClosingTime;
	}

	public void setMorningClosingTime(String morningClosingTime) {
		this.morningClosingTime = morningClosingTime;
	}

	public String getEveningOpeningTime() {
		return eveningOpeningTime;
	}

	public void setEveningOpeningTime(String eveningOpeningTime) {
		this.eveningOpeningTime = eveningOpeningTime;
	}

	public String getEveningClosingTime() {
		return eveningClosingTime;
	}

	public void setEveningClosingTime(String eveningClosingTime) {
		this.eveningClosingTime = eveningClosingTime;
	}

	
	public long getGymId() {
		return gymId;
	}

	public void setGymId(long gymId) {
		this.gymId = gymId;
	}


	@Override
	public String getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}
	public static void main(String[] args) {
	}

}
