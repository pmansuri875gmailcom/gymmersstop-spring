package com.gymmersstop.gd.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.BaseDTO;
@Entity
@Table(name = "gd_gym_review")
public class GymReviewDTO extends BaseDTO {

	public static final String NOT_AVALABLE = "Not Available";
	@Column()
	private long gymId = 0;
	@Column()
	private long userId = 0;
	@Column()
	private double stars = 0;
	@Column(length = 255)
	private String message = null;
			
	
	public long getGymId() {
		return gymId;
	}

	public void setGymId(long gymId) {
		this.gymId = gymId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}


	public double getStars() {
		return stars;
	}

	public void setStars(double stars) {
		this.stars = stars;
	}


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	@Override
	public String getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

}
