package com.gymmersstop.gd.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.BaseDTO;
@Entity
@Table(name = "gd_gym_facilities" )
public class GymFacilitiesDTO extends BaseDTO {
	public static final String AVAILABLE = "available";
	public static final String NOTAVAILABLE = "notavailable";
	@Column()
	private long gymId = 0;
	@Column(length = 255)
	private String ac = NOTAVAILABLE;
	@Column(length = 255)
	private String parking = NOTAVAILABLE;
	@Column(length = 255)
	private String steam = NOTAVAILABLE;
	@Column(length = 255)
	private String minralWater = NOTAVAILABLE;
	@Column(length = 255)
	private String locker = NOTAVAILABLE;
	@Column(length = 255)
	private String dietitian = NOTAVAILABLE;
	//private String availablity = NOTAVAILABLE;
	
	
	
	public long getGymId() {
		return gymId;
	}

	public void setGymId(long gymId) {
		this.gymId = gymId;
	}


	public String getAc() {
		return ac;
	}

	public void setAc(String ac) {
		this.ac = ac;
	}

	public String getParking() {
		return parking;
	}

	public void setParking(String parking) {
		this.parking = parking;
	}

	public String getSteam() {
		return steam;
	}

	public void setSteam(String steam) {
		this.steam = steam;
	}

	public String getMinralWater() {
		return minralWater;
	}

	public void setMinralWater(String minralWater) {
		this.minralWater = minralWater;
	}

	public String getLocker() {
		return locker;
	}

	public void setLocker(String locker) {
		this.locker = locker;
	}

	public String getDietitian() {
		return dietitian;
	}

	public void setDietitian(String dietitian) {
		this.dietitian = dietitian;
	}



	@Override
	public String getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

}
