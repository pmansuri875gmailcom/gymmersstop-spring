package com.gymmersstop.util;

import java.util.HashMap;

/**
 * Class that build Application Email message
 * 
 * @author Tofique Ahmed Khan
 * @version 1.0
 */
public class EmailBuilder {

	/**
	 * Returns Successful User Registration Message
	 * 
	 * @param map
	 *            : Message parameters
	 * @return
	 */

	public static String getGymRegistrationMessage(HashMap<String, String> map) {

		StringBuilder msg = new StringBuilder();

		msg.append("<HTML><BODY>");
		msg.append("Congratulation your Gym is Successfully registered on Gymmers Stop !!!");
		msg.append("<H1>Hi! Greeting from Gymmers Stop Team!</H1>");
		msg.append(
				"<P> You can now access your Gym account online - anywhere, anytime and enjoy the flexibility to check the Details.</P>");
		/*
		 * msg.append("<P><B>Login Id : "+map.get("login")+"<BR>"+" Password : "
		 * +map.get("password")+"</B></P>");
		 */
		/*
		 * msg.append(
		 * "<P> As a security measure, we recommended that you change your password after you first log in.</P>"
		 * );
		 */
		msg.append("</BODY></HTML>");

		return msg.toString();
	}

	public static String getAdminRegistrationMessage(HashMap<String, String> map) {

		StringBuilder msg = new StringBuilder();

		msg.append("<HTML><BODY>");
		msg.append("Congratulation your are Successfully registered on Gymmers Stop as Admin !!!");
		msg.append("<H1>Hi! Greeting from Our Gymmers Stop Team</H1>");
		msg.append(
				"<P>Congratulations for registering on Gymmers Stop! Now you have all the access to over your Gym online.</P>");
		msg.append("<P><B>Login Id : " + map.get("login") + "<BR>" + " Password : " + map.get("password") + "</B></P>");
		msg.append(
				"<P> As a security measure, we recommended that you change your password after you first log in.</P>");
		msg.append("</BODY></HTML>");

		return msg.toString();
	}

	public static String getGymMemberRegistrationMessage(HashMap<String, String> map) {

		StringBuilder msg = new StringBuilder();

		msg.append("<HTML><BODY>");
		msg.append("Congratulation your are Successfully enroll on " + map.get("gymName") + " as Member !!!");
		msg.append("<H1>Hi! Greeting from Gymmers Stop</H1>");
		msg.append("<P>Congratulations for registering on " + map.get("gymName")
				+ " Now you have all the access to over your physical fitness online track your progress .</P>");
		msg.append("<P><B>Login Id : " + map.get("login") + "<BR>" + " Password : " + map.get("password") + "</B></P>");
		msg.append(
				"<P> As a security measure, we recommended that you change your password after you first log in.</P>");
		msg.append("</BODY></HTML>");

		return msg.toString();
	}
	
	public static String getGymStaffRegistrationMessage(HashMap<String, String> map) {

		StringBuilder msg = new StringBuilder();

		msg.append("<HTML><BODY>");
		msg.append("Congratulation your are Successfully Registered on " + map.get("gymName") + " as Member !!!");
		msg.append("<H1>Hi! Greeting from Gymmers Stop</H1>");
		msg.append("<P>Congratulations for registering on " + map.get("gymName")
				+".</P>");
		msg.append("<P><B>Login Id : " + map.get("login") + "<BR>" + " Password : " + map.get("password") + "</B></P>");
		msg.append(
				"<P> As a security measure, we recommended that you change your password after you first log in.</P>");
		msg.append("</BODY></HTML>");

		return msg.toString();
	}

	/**
	 * Returns Email message of Change Password
	 * 
	 * @param map
	 * @return
	 */
	public static String getForgetPasswordMessage(HashMap<String, String> map) {
		StringBuffer msg = new StringBuffer();

		msg.append("<HTML><BODY>");
		msg.append("<H1>Your password is recovered !! " + map.get("firstName") + " " + map.get("lastName") + "</H1>");
		msg.append("<P><B>To access account user login ID : " + map.get("login") + "<BR>" + " Password : "
				+ map.get("password") + "</B></P>");
		msg.append("</BODY></HTML>");
		return msg.toString();
	}

	/**
	 * Returns Email message of Change Password
	 * 
	 * @param map
	 * @return
	 */
	public static String getChangePasswordMessage(HashMap<String, String> map) {
		StringBuffer msg = new StringBuffer();
		msg.append("<HTML><BODY>");
		msg.append("<H1>Your Password has been changed Successfully !! " + map.get("firstName") + " "
				+ map.get("lastName") + "</H1>");
		msg.append("<P><B>To access account user Login Id : " + map.get("login") + "<BR>" + " Password : "
				+ map.get("password") + "</B></P>");
		msg.append("</BODY></HTML>");

		return msg.toString();
	}
}
