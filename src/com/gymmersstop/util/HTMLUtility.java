package com.gymmersstop.util;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.gymmersstop.common.dto.DropdownListBean;

/**
 * HTML Utility class to produce HTML contents like Drop down List.
 * @author Tofique Ahmed Khan
 * @version 1.0
 */
public class HTMLUtility {
	/*
	*//**
	 * Create HTMl Select list from Map parameters values
	 * @param name
	 * @param selectVal
	 * @param map
	 * @return
	 *//*
	public static String getList(String name,String selectVal,HashMap<String, String> map){
		StringBuffer sb = new StringBuffer("<select class='form-control' name='"+name+"'>");
		Set<String> keys = map.keySet();
		String val = null;
		for(String key : keys){
			val = map.get(key);
			if(key.trim().equals(selectVal)){
				sb.append("<option selected value='"+key+"'>"+val+"</option>");
			}else{
				sb.append("<option value='"+key+"'>"+val+"</option>");
			}
		}sb.append("</select>");
		return sb.toString();
	 }
	
	public static String getList(String name, String selectVal, HashMap<String, String> map,boolean select){
		StringBuffer sb = new StringBuffer("<select class='form-control' name='"+name+"'>");
		Set<String> keys = map.keySet();
		String val = null;
		if(select){
			sb.append("<option> selected value=''> --Select-- </option>");
		}
		for(String key : keys){
			val = map.get(key);
			if(key.trim().equals(selectVal)){
				sb.append("<option selected value='"+key+"'>"+val+"</option>");
			}else{
				sb.append("<option value='"+key+"'>"+val+"</option>");
			}
		}sb.append("</subject>");
		return sb.toString();
	}
	
	*//**
	 * Create HTML Select List from list parameter
	 * @param name
	 * @param selectVal
	 * @param list
	 * @return
	 *//*
	
	public static String getList(String name,String selectVal,List list){
		Collections.sort(list);
		List<DropdownListBean> dd = (List<DropdownListBean>) list;
		StringBuffer sb = new StringBuffer("<select class='form-control' name='"+name+"'>");
		String key = null;
		String val = null;
		
		for(DropdownListBean obj: dd){
			key = obj.getKey();
			val = obj.getValue();
			
			if(key.trim().equals(selectVal)){
				sb.append("<option selected value='"+key+"'>"+val+"</option>");
			}else{
				sb.append("<option value='"+key+"'>"+val+"</option>");
			}
		}sb.append("</select>");
		return sb.toString();
	}

	*//**
	 * Returns all Input Validation error messages as UL List	
	 * @param request
	 * @return
	 *//*
	public static String getInputErrorsMessages(HttpServletRequest request){
		Enumeration<String> e = request.getAttributeNames();
		
		StringBuffer sb = new StringBuffer("<UL>");
		String name = null;
		
		while (e.hasMoreElements()) {
			name = (String) e.nextElement();
			if(name.startsWith("error.")){
				sb.append("<LI class='error'>"+request.getAttribute("name")+"</LI>");
			}
		}
		sb.append("</UL>");
		return sb.toString();
	}
	
	*//**
	 * Returns Error Message with HTML tag and CSS
	 * @param request
	 * @return
	 *//*
	public static String getErrorMessage(HttpServletRequest request){
		String msg = ServletUtility.getErrorMessage(request);
		if(!DataValidator.isNull(msg)){
			msg = "<p>"+msg+"</p>";
		}
		return msg;
	}
	
	*//**
	 * Returns Success Message with HTML tag and CSS
	 * @param request
	 * @return
	 *//*
	public static String getSuccessMessage(HttpServletRequest request){
		String msg = ServletUtility.getSuccessMessage(request);
		if(!DataValidator.isNull(msg)){
			msg = "<p class='st-success-header'>"+msg+"</p>";
		}
		return msg;
	}
	
	*//**
	 * Creates submit button if user has access permission.
	 * @param label
	 * @param access
	 * @param request
	 * @return
	 *//*
	public static String getSubmitButton(String label, boolean access,HttpServletRequest request){
		String button = "";
		if(access){
			button="<input type='submit' name='operation' value='"+label+"'>";
		}
		return button;
	}
	
	public static String getCommonFields(HttpServletRequest request){
		return null;
	}*/
}
