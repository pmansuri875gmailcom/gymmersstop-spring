package com.gymmersstop.util;

import java.util.ResourceBundle;

/**
 * Read the property values from application properties file using Resource Bundle
 * @author Tofique Ahmed Khan
 * @version 1.0
 */
public class PropertyReader {


	private static ResourceBundle rb = ResourceBundle.getBundle("com.gymmersstop.bundle.system");
	
	/**
	 * Return value of key
	 * @param val
	 * @return
	 */
	public static String getValue(String key){
		String val = null;
		try{
			val = rb.getString(key);
		}catch (Exception e) {
			val = key;
		}
		return val;
	}
	
	/**
	 * Gets String after placing param valuess
	 * @param key
	 * @param param
	 * @return
	 */
	public static String getValue(String key, String param){
		String msg = getValue(key);
		msg = msg.replace("{0}", param);
		return msg;
	}
	
}
