package com.gymmersstop.util;

import java.util.Date;

/**
 * This class validates input data
 * @author Tofique Ahmed Khan
 * @version 1.0
 */
public class DataValidator {

	/**
	 * Checks if value is Null
	 * @param val
	 * @return
	 */
	public static boolean isNull(String val){
		if(val == null || val.trim().length() == 0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Checks if value is Not Null
	 * @param val
	 * @return
	 */
	public static boolean isNotNull(String val){
		return !isNull(val);
	}
	
	/**
	 * Checks if value is Integer
	 * @param val
	 * @return
	 */
	public static boolean isInteger(String val){
		if(isNotNull(val)){
			try{
				int i = Integer.parseInt(val);
				return true;
			}catch(NumberFormatException e){
				return false;
			}
		}else{
			return false;
		}
	}
	
	/**
	 * Checks if value is long
	 * @param val
	 * @return
	 */
	public static boolean isLong(String val){
		if(isNotNull(val)){
			try{
				long l = Long.parseLong(val);
				return true;
			}catch (NumberFormatException e) {
				return false;
			}
		}else{
			return false;
		}
	}
	
	public static boolean isDouble(String val){
		if(isNotNull(val)){
			try{
				double d = Double.parseDouble(val);
				return true;
			}catch (NumberFormatException e) {
				return false;
			}
		}else{
			return false;
		}
	}
	
	/**
	 * Checks if value is valid Email ID
	 * @param val
	 * @return
	 */
	public static boolean isEmail(String val){
		String emailreg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		if(isNotNull(val)){
			try{
				return val.matches(emailreg);
			}catch (NumberFormatException e) {
				return false;
			}
		}else{
			return false;
		}
	}
	
	/**
	 * Checks if value is Date
	 * @param val
	 * @return
	 */
	public static boolean isDate(String val){
		Date d = null;
		if(isNotNull(val)){
			d = DataUtility.getDate(val);
		}
		return d != null;
	}
	
	/**
	*Check if value is name
	*
	*@param val
	*@return
	*
	*/
	public static boolean isName(String val){

	if(isNotNull(val)){
		String namereg="^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$";
		if(val.matches(namereg)){
			return true;
		}else{
			return false;
		}
		
	}else{return false;}

	}
	
	/**
	*
	*Checks if input value is valid mobileNumber
	*
	*@param val
	*@return
	*/

	public static boolean isMobileNumber(String val){

	if(isNotNull(val)){
	String mobilereg="(0/91)?[6-9][0-9]{9,11}";	
		if(val.matches(mobilereg)){
		return true;
		}else{
		return false;
		}
	}else{
	return false;
	}

	}
	public static void main(String[] args) {
		System.err.println(isName("hasnain123"));
	}
	
}

