package com.gymmersstop.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Data Utility class to format data form one format to another
 * @author Tofique Ahmed Khan
 * @version 1.0
 *
 */
public class DataUtility {

	/**
	 * Application Date Format
	 */
	public static final String APP_DATE_FORMAT = PropertyReader.getValue("format.date"); 
	public static final String APP_TIME_FORMAT = PropertyReader.getValue("format.datetime");
	
	/**
	 * Date formatter
	 */
	private static final SimpleDateFormat formatter =new SimpleDateFormat(APP_DATE_FORMAT);
	private static final SimpleDateFormat timeFormatter = new SimpleDateFormat(APP_TIME_FORMAT);
	
	/**
	 * Trim trailing and leading spaces of String
	 * @param val
	 * @return
	 */
	public static String getString(String val){
		if(DataValidator.isNotNull(val)){
			return val.trim();
		}else{
			return val;
		}
	}
	
	/**
	 * Converts Object to String
	 * @param val
	 * @return
	 */
	public static String getStringData(Object val){
		if(val !=null){
			return val.toString();
		}else{
			return "";
		}
	}
	
	/**
	 * Converts String into Integer
	 * @param val
	 * return 
	 */
	public static int getInteger(String val){
		if(DataValidator.isInteger(val)){
			return Integer.parseInt(val);
		}else{
			return 0;
		}
	}
	
	/**
	 * Converts String into Long
	 * @param val
	 * @return
	 */
	public static long getLong(String val){
		if(DataValidator.isLong(val)){
			return Long.parseLong(val);
		}else{
			return 0;
		}
	}
	
	public static double getDouble(String val){
		if(DataValidator.isDouble(val)){
			return Double.parseDouble(val);
		}else{
			return 0.0;
		}
	}
	
	/**
	 * Converts String into Date
	 * @param val
	 * @return
	 */
	public static Date getDate(String val){
		Date date = null;
		try{
			date = formatter.parse(val);
		}catch(Exception e){
			
		}
		return date;
	}
	
	/**
	 * Converts Date into String
	 * @param date
	 * @return
	 */
	public static String getDateString(Date date){
		try{
			return formatter.format(date);
		}catch(Exception e){
			return "";
		}
	}
	
	/**
	 * Gets date after n number of days
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date getDate(Date date,int day){
		
		
		return null;
	}
	
	/**
	 * Converts String into Time
	 * @param val
	 * @return
	 */
	public static Timestamp getTimestamp(String val){
		Timestamp timeStamp = null;
		try{
			timeStamp = new Timestamp(timeFormatter.parse(val).getTime());
		}catch(Exception e){
			return null;
		}
		return timeStamp;
	}
	
	public static Timestamp getTimestamp(long l){
		Timestamp timeStamp = null;
		try{
			timeStamp = new Timestamp(l);
		}catch(Exception e){
			return null;
		}
		return timeStamp;
	}
	
	public static Timestamp getCurrentTimestamp(){
		Timestamp timeStamp = null;
		try{
			timeStamp = new Timestamp(new Date().getTime());
		}catch(Exception e){
			return null;
		}
		return timeStamp;
	}
	
	public static long getTimestamp(Timestamp tm){
		try{
			return tm.getTime();
		}catch(Exception e){
			return 0;
		}
	}
	
	public static Date getCurrentDate(){
		return new Date();
	}
	public static void main(String[] args) {
		String date = getDateString(new Date());
		System.out.println(date);
	
		date = date.replace("/", "-");
		System.out.println(date);
		
	}
}
