package com.gymmersstop.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

/*import com.gymmersstop.common.dto.*;
import com.gymmersstop.common.controller.GMSView;
import com.gymmersstop.common.model.AppRole;*/

public class MenuBuilder {

	/*public static final int HORIZONTIAL = 1;
	public static final int VERTICAL = 2;
	public static final String separator = "";

	public static String getLink(String text, String url) {
		return "<a href= '" + url + "'>" + text + "</a>";
	}
	
	public static String getBootstrapNavLink(String text,String url){
		return "<a class='nav-link' href='"+url+"'>"+text+"</a>";
	}

	public static String getHorizontalLink(HashMap<String, String> hmap) {
		StringBuffer sb = new StringBuffer();
		Iterator<String> keys = hmap.keySet().iterator();

		String key = null;
		String value = null;
		while (keys.hasNext()) {
			key = keys.next();
			value = hmap.get(key);
			sb.append("<li class='na'>"+getBootstrapNavLink(key, value)+"</li>");
		}
		return sb.toString();
	}

	public static String getVerticalLink(HashMap<String, String> hmap) {
		StringBuffer sb = new StringBuffer("<UL>");
		Iterator<String> keys = hmap.keySet().iterator();
		String key = null;
		String value = null;
		while (keys.hasNext()) {
			key = keys.next();
			value = hmap.get(key);
			sb.append("<LI>" + getLink(key, value) + "</LI>");
		}
		sb.append("</UL>");
		return sb.toString();
	}

	public static String getMenu(long roleId) {
		return getMenu(roleId, HORIZONTIAL);
	}

	public static String getMenu(long roleId, int i) {
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("Dashboard", GMSView.DASHBOARD_CTL);
		
		if (roleId == AppRole.STAFF || roleId == AppRole.ADMIN) {
			map.put("Add Visitor", GMSView.VISITOR_CTL);
			map.put("Add GymMember", GMSView.GYM_MEMBER_CTL);
			map.put("Add Staff", GMSView.STAFF_CTL);
			map.put("Visitor List", GMSView.VISITOR_LIST_CTL);
			map.put("Gym Member List", GMSView.GYMMER_LIST_CTL);
			map.put("Staff List", GMSView.STAFF_LIST_CTL);
			map.put("MyProfile", GMSView.MY_PROFILE_CTL);
			map.put("Change Password", GMSView.CHANGE_PASSWORD_CTL);
		} else if (roleId == AppRole.GYMMEMBER) {
			map.put("MyProfile", GMSView.MY_PROFILE_CTL);
			map.put("Change Password", GMSView.CHANGE_PASSWORD_CTL);
		}

		if (i == HORIZONTIAL) {
			return getHorizontalLink(map);
		} else {
			return getVerticalLink(map);
		}
	}

	public static String getMenu1(long roleId){
		StringBuffer sb = new StringBuffer();
		LinkedHashMap<String, String> map = new LinkedHashMap<String,String>();
		if(roleId == AppRole.STAFF || roleId == AppRole.ADMIN){
			map.put("User List", GMSView.USER_LIST_CTL);
			map.put("Add Role", GMSView.ROLE_CTL);
			map.put("Role List", GMSView.ROLE_LIST_CTL);
			map.put("Change Password", GMSView.CHANGE_PASSWORD_CTL);
			map.put("MyProfile", GMSView.MY_PROFILE_CTL);
//			map.put("Gymmer Information", GMSView.GYM);
			map.put("Gymmer List", GMSView.GYMMER_LIST_CTL);
			map.put("Staff", GMSView.STAFF_CTL);
			map.put("Staff List", GMSView.STAFF_LIST_CTL);
			map.put("Logout", GMSView.LOGIN_CTL);
		}else if(roleId == AppRole.GYMMEMBER){
			map.put("MyProfile", GMSView.MY_PROFILE_CTL);
			map.put("Change Password", GMSView.CHANGE_PASSWORD_CTL);
			map.put("Logout", GMSView.LOGIN_CTL);
		}
		return getHorizontalLink(map);
	}

	
	public static String getGDMenu(long roleId,UserDTO bean){
		
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		String gdMenu = null;
		map.put("Home", GMSView.DISCOVERY_CTL);
		map.put("About us",GMSView.GD_ABOUT_US_VIEW);
		map.put("Contact us", GMSView.GD_CONTACT_US_VIEW);
		
	if (roleId == AppRole.GDGYMADMIN || roleId == AppRole.GDVISITOR) {
			gdMenu = getHorizontalLink(map)+"<div class='btn-group'><button type='button' class='btn btn-secondary dropdown-toggle' data-toggle='dropdown' data-display='static' aria-haspopup='true' aria-expanded='false'>"+
bean.getEmail()+"  </button>"
+ "  <div class='dropdown-menu'><a class='dropdown-item' href='"+GMSView.GD_VISITOR_PROFILE_CTL+"' >Profile</a>"
+""	
		+ "<a class='dropdown-item' href='"+GMSView.GYM_DISCOVERY_REGISTRATION+"?value=logout' >Log out</a>"
  +"</div></div>"
  ;
		} else if (roleId == AppRole.GUEST) {
			 gdMenu =  getHorizontalLink(map)+"<li class='nav-item mr-2'><a type='button' class='btn btn-warning font-weight-bold' data-toggle='modal' data-target='#loginModal'>Sign in</a></li>"+
					"<li class='nav-item mr-2'><a class='btn btn-primary font-weight-bold' href='"+GMSView.GYM_LISTING_REGISTRATION+"'>Free Gym Listing</a></li>"
					+"<li class='nav-item mr-2'><a class='btn btn-outline-light font-weight-bold' href='"+GMSView.LOGIN_CTL+"'>Management Software</a></li>";
		}

		
		return gdMenu;
	}
	
	public static String getGDSettingLink(String text,String url){
		return "<a href='"+url+"'>"+text+"</a>";
		
		 
	}
	
	public static String getGDSettingLink(HashMap<String, String> hmap) {
		StringBuffer sb = new StringBuffer("<UL class = 'list-unstyled'>");
		Iterator<String> keys = hmap.keySet().iterator();

		String key = null;
		String value = null;
		while (keys.hasNext()) {
			key = keys.next();
			value = hmap.get(key);
			sb.append("<li>"+getBootstrapNavLink(key, value)+"</li>");
		}
		sb.append("</UL>");
		return sb.toString();
	}
	
	
	public static String getGDSettingMenu(long roleId){
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();

		if (roleId == AppRole.GDGYMADMIN) {
			map.put("Owner Profile", GMSView.GD_VISITOR_PROFILE_CTL);
			map.put("Gym Profile", GMSView.GD_PROFILE_CTL);
			map.put("Gym Gallery", GMSView.GYM_UPLOAD_GALLERY_CTL);
			map.put("Facalities", GMSView.GD_FACILITIES_CTL);
			map.put("Gym Timing", GMSView.GD_GYM_TIMING_CTL);
			
		}
		map.put("Change Password", GMSView.GD_CHANGE_PASSWORD_CTL);
		if (roleId == AppRole.GDVISITOR) {
			map.put("General Profile Setting", GMSView.GD_VISITOR_PROFILE_CTL);
		}
		
		
		return getGDSettingLink(map);
	}
	
	*/
	
}
