package com.gymmersstop.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

public class ShowImage {

	public static String showImage(String path){
		String b64 = null;
		try {
				System.out.println(new File(path));
				
				BufferedImage bImage = ImageIO.read(new File(path));//give the path of an image
		        ByteArrayOutputStream baos = new ByteArrayOutputStream();
		        ImageIO.write( bImage, "png", baos );
		        baos.flush();
		        byte[] imageInByteArray = baos.toByteArray();
		        baos.close();                                   
		        b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return b64;
	}
	
	public static String showLogo(String path){
		String b64 = null;
		try {
				BufferedImage bImage = ImageIO.read(new File(path));//give the path of an image
		        ByteArrayOutputStream baos = new ByteArrayOutputStream();
		        ImageIO.write( bImage, "png", baos );
		        baos.flush();
		        byte[] imageInByteArray = baos.toByteArray();
		        baos.close();                                   
		        b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return b64;
	}
	
	
	
}
