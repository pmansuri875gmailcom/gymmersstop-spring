package com.gymmersstop.up.dao;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.up.dto.UserTrackingDTO;

public interface UserTrackingDaoInterface {
	
	public long add(UserTrackingDTO dto)throws ApplicationException;
	public void delete(UserTrackingDTO dto)throws ApplicationException;
	public void update(UserTrackingDTO dto)throws ApplicationException;
	public UserTrackingDTO  findByPk(long pk)throws ApplicationException;

}
