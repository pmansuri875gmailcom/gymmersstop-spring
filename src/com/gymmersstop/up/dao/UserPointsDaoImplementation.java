package com.gymmersstop.up.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.up.dto.UserPointsDTO;

@Repository("userPointsDao")
public class UserPointsDaoImplementation implements UserPointsDaoInterface {
	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(UserPointsDTO dto) throws ApplicationException {
		return (long)ht.save(dto);
	}

	@Override
	public void delete(UserPointsDTO dto) throws ApplicationException {

		ht.delete(dto);
	}

	@Override
	public void update(UserPointsDTO dto) throws ApplicationException {
		ht.update(dto);
	}

	@Override
	public UserPointsDTO findByPk(long pk) throws ApplicationException {
		return (UserPointsDTO) ht.get(UserPointsDTO.class, pk );
	}

}
