package com.gymmersstop.up.dao;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.up.dto.UserPointsDTO;

public interface UserPointsDaoInterface {

	public long add(UserPointsDTO dto)throws ApplicationException;
	public void delete(UserPointsDTO dto)throws ApplicationException;
	public void update(UserPointsDTO dto)throws ApplicationException;
	public UserPointsDTO  findByPk(long pk)throws ApplicationException;
}
