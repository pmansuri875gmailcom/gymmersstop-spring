package com.gymmersstop.up.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.up.dto.UserTrackingDTO;

@Repository("userTrackingDao")
public class UserTrackingDaoImplementation implements UserTrackingDaoInterface {

	@Autowired
	private HibernateTemplate ht;

	@Override
	public long add(UserTrackingDTO dto) throws ApplicationException {
		return (long) ht.save(dto);
	}

	@Override
	public void delete(UserTrackingDTO dto) throws ApplicationException {
		ht.delete(dto);

	}

	@Override
	public void update(UserTrackingDTO dto) throws ApplicationException {
		ht.update(dto);
	}

	@Override
	public UserTrackingDTO findByPk(long pk) throws ApplicationException {
		
		return (UserTrackingDTO)ht.get(UserTrackingDTO.class, pk);
	}

}
