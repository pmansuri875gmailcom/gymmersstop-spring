package com.gymmersstop.up.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gymmersstop.common.dto.BaseDTO;
@Entity
@Table(name ="up_user_tracking" )
public class UserTrackingDTO extends BaseDTO {
	

	@Column
	private long userId = 0;
	@Column
	private long stepCounts = 0;
	@Column
	private Date date = null;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getStepCounts() {
		return stepCounts;
	}

	public void setStepCounts(long stepCounts) {
		this.stepCounts = stepCounts;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


	@Override
	public String getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

}
