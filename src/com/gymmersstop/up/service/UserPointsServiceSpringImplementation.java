package com.gymmersstop.up.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.common.dao.UserDAOInterface;
import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.up.dao.UserPointsDaoInterface;
import com.gymmersstop.up.dto.UserPointsDTO;

public class UserPointsServiceSpringImplementation implements UserPointsServiceInterface {
	@Autowired
	private UserPointsDaoInterface dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public long add(UserPointsDTO dto) throws ApplicationException {
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(UserPointsDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(UserPointsDTO dto) throws ApplicationException {
		dao.update(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserPointsDTO findByPk(long pk) throws ApplicationException {
		return dao.findByPk(pk);
	}

}
