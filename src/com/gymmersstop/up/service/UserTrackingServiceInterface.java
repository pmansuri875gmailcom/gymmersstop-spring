package com.gymmersstop.up.service;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.up.dto.UserTrackingDTO;

public interface UserTrackingServiceInterface {
	public long add(UserTrackingDTO dto)throws ApplicationException;
	public void delete(UserTrackingDTO dto)throws ApplicationException;
	public void update(UserTrackingDTO dto)throws ApplicationException;
	public UserTrackingDTO  findByPk(long pk)throws ApplicationException;
}
