package com.gymmersstop.up.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gymmersstop.exception.ApplicationException;
import com.gymmersstop.up.dao.UserTrackingDaoInterface;
import com.gymmersstop.up.dto.UserTrackingDTO;

public class UserTrackingServiceSpringImplentation implements UserTrackingServiceInterface {

	@Autowired
	private UserTrackingDaoInterface dao;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public long add(UserTrackingDTO dto) throws ApplicationException {
		// TODO Auto-generated method stub
		return dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(UserTrackingDTO dto) throws ApplicationException {
		dao.delete(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(UserTrackingDTO dto) throws ApplicationException {
		dao.add(dto);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserTrackingDTO findByPk(long pk) throws ApplicationException {
		return dao.findByPk(pk);
	}

}
