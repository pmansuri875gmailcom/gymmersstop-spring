package com.gymmersstop.test;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gymmersstop.common.dao.RoleDAOImplementation;
import com.gymmersstop.common.dao.RoleDAOInterface;
import com.gymmersstop.common.dto.RoleDTO;
import com.gymmersstop.common.service.RoleServiceInterface;
import com.gymmersstop.exception.DuplicateRecordException;



public class RoleTest {
	
public static void main(String[] args) {
	
	ApplicationContext ap  = new ClassPathXmlApplicationContext("resources/Spring.xml");
	RoleDAOInterface rf = ap.getBean(RoleDAOImplementation.class);
	RoleDTO dto  = new RoleDTO();
	dto.setId(101);
	List list = rf.search(dto, 0, 0);
	System.out.println("hello" + list);
}
	

}
